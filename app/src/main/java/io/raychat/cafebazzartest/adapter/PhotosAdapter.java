package io.raychat.cafebazzartest.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import io.raychat.cafebazzartest.R;
import io.raychat.cafebazzartest.model.detail.Item;
import io.raychat.cafebazzartest.model.explore.PhotoGroup;

/**
 * Created by Alireza Nezami on 4/8/2021.
 */
public class PhotosAdapter extends BaseAdapter<Item> {

    private FooterViewHolder footerViewHolder;
    public final Context context;
    private LayoutInflater itemInflater;
    private ViewGroup.LayoutParams layoutParams;

    public PhotosAdapter(Context context, ViewGroup.LayoutParams layoutParams) {
        super();
        this.context = context;
        this.layoutParams = layoutParams;
        itemInflater = LayoutInflater.from(context);

    }


    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }


    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        View v = itemInflater.inflate(R.layout.item_photos, parent, false);
        v.setLayoutParams(layoutParams);
        final VenueViewHolder holder = new VenueViewHolder(v, itemInflater);

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int adapterPos = holder.getAdapterPosition();
//                if (adapterPos != RecyclerView.NO_POSITION) {
//                    if (onItemClickListener != null) {
//                        onItemClickListener.onItemClick(adapterPos, holder.itemView);
//                    }
//                }
//            }
//        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        View v = itemInflater.inflate(R.layout.item_explore_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(v, itemInflater);
//        holder.reloadButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (onReloadClickListener != null) {
//                    onReloadClickListener.onReloadClick();
//                }
//            }
//        });

        return holder;
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final VenueViewHolder holder = (VenueViewHolder) viewHolder;

        Item item = getItem(position);
        if (item != null) {
            holder.bind(item);
        }
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterViewHolder holder = (FooterViewHolder) viewHolder;
        footerViewHolder = holder;
    }

    @Override
    protected void displayLoadMoreFooter() {
//        if (footerViewHolder != null) {
//            footerViewHolder.errorRelativeLayout.setVisibility(View.GONE);
//            footerViewHolder.loadingFrameLayout.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    protected void displayErrorFooter() {
//        if (footerViewHolder != null) {
//            footerViewHolder.loadingFrameLayout.setVisibility(View.GONE);
//            footerViewHolder.errorRelativeLayout.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new Item());
    }

    // region Inner Classes

    public static class VenueViewHolder extends RecyclerView.ViewHolder {

        ImageView imgPhoto;

        public VenueViewHolder(View view, LayoutInflater inflater) {
            super(view);

            imgPhoto = view.findViewById(R.id.imgPhoto);

        }


        // region Helper Methods
        private void bind(Item item) {
            showImage(item);
        }

        private void showImage(Item item) {
            String url = item.getPhotoUrl("original");
            if (!TextUtils.isEmpty(url)) {
                Glide.with(imgPhoto.getContext())
                        .asBitmap()
                        .load(url)
                        .format(DecodeFormat.PREFER_RGB_565)
                        .apply(new RequestOptions().override(70, 70).centerCrop())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.img_explore_placeholder)
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                imgPhoto.setImageBitmap(resource);
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {

                            }
                        });
            }

        }


    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        public FooterViewHolder(View view, LayoutInflater inflater) {
            super(view);
        }


    }


}
