package io.raychat.cafebazzartest.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.ArrayList;
import java.util.List;

import io.raychat.cafebazzartest.model.detail.Photos;
import io.raychat.cafebazzartest.ui.detail.PhotosFragment;

public class ViewPager2Adapter extends FragmentStateAdapter {

    private List<Fragment> fragmentList = new ArrayList<>();

    public ViewPager2Adapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    public void setFragmentList(List<Fragment> fragmentList) {
        this.fragmentList = fragmentList;
    }

    public void updatePhotosFragment(Photos photos) {
        if (photos != null) {
            PhotosFragment photosFragment = (PhotosFragment) fragmentList.get(0);
            photosFragment.updateList(photos);
        }
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getItemCount() {
        return fragmentList.size();
    }
}
