package io.raychat.cafebazzartest.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;
import java.util.Locale;

import io.raychat.cafebazzartest.R;
import io.raychat.cafebazzartest.model.explore.GroupItem;
import io.raychat.cafebazzartest.model.explore.Tips;
import io.raychat.cafebazzartest.model.explore.Venue;
import io.raychat.cafebazzartest.utils.customView.roundLayout.RoundedLinearLayout;

/**
 * Created by Alireza Nezami on 4/8/2021.
 */
public class VenuesAdapter extends BaseAdapter<GroupItem> {

    private FooterViewHolder footerViewHolder;
    public final Context context;
    private LayoutInflater itemInflater;


    public VenuesAdapter(Context context) {
        super();
        this.context = context;
        itemInflater = LayoutInflater.from(context);

    }


    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }


    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        View v = itemInflater.inflate(R.layout.item_explore, parent, false);

        final VenueViewHolder holder = new VenueViewHolder(v, itemInflater);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int adapterPos = holder.getAdapterPosition();
                if (adapterPos != RecyclerView.NO_POSITION) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(getItem(adapterPos));
                    }
                }
            }
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        View v = itemInflater.inflate(R.layout.item_explore_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(v, itemInflater);
//        holder.reloadButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (onReloadClickListener != null) {
//                    onReloadClickListener.onReloadClick();
//                }
//            }
//        });

        return holder;
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final VenueViewHolder holder = (VenueViewHolder) viewHolder;

        GroupItem item = getItem(position);
        if (item != null) {
            holder.bind(item.getVenue(), item.getTips(), position);
        }
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterViewHolder holder = (FooterViewHolder) viewHolder;
        footerViewHolder = holder;
    }

    @Override
    protected void displayLoadMoreFooter() {
//        if (footerViewHolder != null) {
//            footerViewHolder.errorRelativeLayout.setVisibility(View.GONE);
//            footerViewHolder.loadingFrameLayout.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    protected void displayErrorFooter() {
//        if (footerViewHolder != null) {
//            footerViewHolder.loadingFrameLayout.setVisibility(View.GONE);
//            footerViewHolder.errorRelativeLayout.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new GroupItem());
    }

    // region Inner Classes

    public static class VenueViewHolder extends RecyclerView.ViewHolder {

        //        private ItemExploreBinding binding;
        TextView txtRatingAvg;
        TextView txtTotalVotes;
        TextView txtName;
        TextView txtCategoryDistanceIsOpen;
        TextView txtFullAddress;
        TextView txtCheckIns;
        TextView txtUsersCount;
        TextView txtTipCount;
        TextView txtLastTip;
        RoundedLinearLayout lytRating;
        ImageView imgVenuePhoto;

        public VenueViewHolder(View view, LayoutInflater inflater) {
            super(view);
//            binding = ItemExploreBinding.inflate(inflater);

            txtRatingAvg = view.findViewById(R.id.txtRatingAvg);
            txtTotalVotes = view.findViewById(R.id.txtTotalVotes);
            txtName = view.findViewById(R.id.txtName);
            txtCategoryDistanceIsOpen = view.findViewById(R.id.txtCategoryDistanceIsOpen);
            txtFullAddress = view.findViewById(R.id.txtFullAddress);
            txtTipCount = view.findViewById(R.id.txtTipCount);
            txtUsersCount = view.findViewById(R.id.txtUsersCount);
            txtCheckIns = view.findViewById(R.id.txtCheckIns);
            txtLastTip = view.findViewById(R.id.txtLastTip);
            lytRating = view.findViewById(R.id.lytRating);
            imgVenuePhoto = view.findViewById(R.id.imgVenuePhoto);

        }


        // region Helper Methods
        private void bind(Venue item, List<Tips> tips, int position) {
            showImage(item);
            showLastTip(tips);
            showName(item, position);
            if (item.getRating() != null && item.getRatingColor() != null && item.getRatingSignals() != null) {
                txtTotalVotes.setVisibility(View.VISIBLE);
                lytRating.setClippedBackgroundColor(Color.parseColor("#" + item.getRatingColor()));
                txtRatingAvg.setText(item.getRating());
                txtTotalVotes.setText(String.format("Votes: %s", item.getRatingSignals()));
            }
            if (item.isVerified()) {
                txtName.setCompoundDrawablesWithIntrinsicBounds(null,
                        null,
                        ResourcesCompat.getDrawable(txtName.getContext().getResources(), R.drawable.ic_verified, null),
                        null);
            }

            StringBuilder categoryNames = new StringBuilder();
            for (int i = 0; i < item.getCategories().size(); i++) {
                categoryNames
                        .append(item.getCategories().get(i).getName())
                        .append(i != 0 ? " | " : "");
            }

//            String distance = AndroidUtilities.distanceBetweenTwoPoint()
            String isOpen = "";
            if (item.getHours() != null)
                isOpen = item.getHours().getIsOpen() ? "Open Now" : "Closed Now";

            txtCategoryDistanceIsOpen.setText(String.format("%s %s",
                    categoryNames,
                    isOpen));

            if (item.getStats() != null) {
                txtTipCount.setText(item.getStats().getTipCount());
                txtUsersCount.setText(item.getStats().getUsersCount());
                txtCheckIns.setText(item.getStats().getCheckinsCount());
            } else {
                txtTipCount.setVisibility(View.GONE);
                txtUsersCount.setVisibility(View.GONE);
                txtCheckIns.setVisibility(View.GONE);
            }

            //address
            if (item.getLocation() != null)
                txtFullAddress.setText(String.format("%s, %s", item.getLocation().getCity(), item.getLocation().getAddress()));
        }

        private void showName(Venue item, int position) {
            Spannable word = new SpannableString(String.format(Locale.getDefault(), "%1d. ", position + 1));
            word.setSpan(new ForegroundColorSpan(txtName.getContext().getResources().getColor(R.color.blue300)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txtName.setText(word);
            txtName.append(item.getName());
        }

        private void showLastTip(List<Tips> tips) {
            if (tips != null && tips.size() > 0) {
                Tips tip = tips.get(0);
                txtLastTip.setVisibility(View.VISIBLE);
                StringBuilder lastTip = new StringBuilder();
                if (tip.getUser() != null) {
                    Spannable word = new SpannableString(String.format("%s says: ", tip.getUser().getFirstName()));
                    word.setSpan(new ForegroundColorSpan(txtLastTip.getContext().getResources().getColor(R.color.blue200)), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    txtLastTip.setText(word);
                }
                lastTip.append(tip.getText());
                txtLastTip.append(tip.getText());
            } else {
                txtLastTip.setVisibility(View.GONE);
            }
        }

        private void showImage(Venue item) {
            if (item.getPhotos() != null && item.getPhotos().getGroups() != null && item.getPhotos().getGroups().size() > 0) {
                String url = item.getPhotos().getGroups().get(0).getPhotoUrl("original");
                if (!TextUtils.isEmpty(url)) {
                    Glide.with(imgVenuePhoto.getContext())
                            .asBitmap()
                            .load(url)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.img_explore_placeholder)
                            .into(new CustomTarget<Bitmap>() {
                                @Override
                                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                    imgVenuePhoto.setImageBitmap(resource);
                                }

                                @Override
                                public void onLoadCleared(@Nullable Drawable placeholder) {

                                }
                            });
                }
            }
        }

        private void setUpTitle(TextView tv, GroupItem item) {
//            String formattedCategoryName = item.getFormattedCategoryName();
//            if (!TextUtils.isEmpty(formattedCategoryName)) {
//                tv.setText(formattedCategoryName);
//            }
        }

    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        public FooterViewHolder(View view, LayoutInflater inflater) {
            super(view);
        }


    }


}
