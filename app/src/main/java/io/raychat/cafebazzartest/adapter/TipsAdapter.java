package io.raychat.cafebazzartest.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.shimmer.ShimmerFrameLayout;

import io.raychat.cafebazzartest.R;
import io.raychat.cafebazzartest.model.detail.Item;

/**
 * Created by Alireza Nezami on 4/8/2021.
 */
public class TipsAdapter extends BaseAdapter<Item> {

    private FooterViewHolder footerViewHolder;
    public final Context context;
    private LayoutInflater itemInflater;

    public TipsAdapter(Context context) {
        super();
        this.context = context;
        itemInflater = LayoutInflater.from(context);

    }


    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : ITEM;
    }


    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        View v = itemInflater.inflate(R.layout.item_tips, parent, false);
        return new VenueViewHolder(v);
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {
        View v = itemInflater.inflate(R.layout.item_explore_footer, parent, false);
        return new FooterViewHolder(v, itemInflater);
    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final VenueViewHolder holder = (VenueViewHolder) viewHolder;

        Item item = getItem(position);
        if (item != null) {
            holder.bind(item);
        }
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        FooterViewHolder holder = (FooterViewHolder) viewHolder;
        footerViewHolder = holder;
    }

    @Override
    protected void displayLoadMoreFooter() {
//        if (footerViewHolder != null) {
//            footerViewHolder.errorRelativeLayout.setVisibility(View.GONE);
//            footerViewHolder.loadingFrameLayout.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    protected void displayErrorFooter() {
//        if (footerViewHolder != null) {
//            footerViewHolder.loadingFrameLayout.setVisibility(View.GONE);
//            footerViewHolder.errorRelativeLayout.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public void addFooter() {
        isFooterAdded = true;
        add(new Item());
    }

    // region Inner Classes

    public static class VenueViewHolder extends RecyclerView.ViewHolder {

        ImageView imgAvatar;
        TextView txtName;
        TextView txtDate;
        TextView txtTipText;
        ShimmerFrameLayout shimmer;

        public VenueViewHolder(View view) {
            super(view);

            imgAvatar = view.findViewById(R.id.imgAvatar);
            txtName = view.findViewById(R.id.txtName);
            txtDate = view.findViewById(R.id.txtDate);
            txtTipText = view.findViewById(R.id.txtTipText);
            shimmer = view.findViewById(R.id.shimmer);
        }


        // region Helper Methods
        private void bind(Item item) {
            showImage(item);

            if (item.getUser() != null) {
                txtName.setText(String.format("%s %s.", item.getUser().getFirstName(), item.getUser().getLastName()));
            }
            txtDate.setText(item.getDate());
            txtTipText.setText(item.getText());

            shimmer.hideShimmer();
        }

        private void showImage(Item item) {
            String url = item.getPhotoUrl("original");

            Glide.with(imgAvatar.getContext())
                    .asBitmap()
                    .load(url)
                    .format(DecodeFormat.PREFER_RGB_565)
                    .apply(new RequestOptions().override(70, 70).centerCrop())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_placeholder)
                    .into(new CustomTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            imgAvatar.setImageBitmap(resource);
                        }

                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                            imgAvatar.setImageDrawable(placeholder);
                        }
                    });


        }


    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        public FooterViewHolder(View view, LayoutInflater inflater) {
            super(view);
        }


    }


}
