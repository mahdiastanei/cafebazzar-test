package io.raychat.cafebazzartest;

import android.app.Application;
import android.content.Context;

import androidx.appcompat.app.AppCompatDelegate;

import io.raychat.cafebazzartest.utils.room.LocalDatabase;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class App extends Application {

    public static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        initAppDatabase(this);

        // for changing vector drawable inner color
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }

    private void initAppDatabase(Context context) {
        LocalDatabase localDatabase = new LocalDatabase(context);
        localDatabase.initAppDatabase();


    }

    public static App getInstance() {
        return app;
    }

}
