
package io.raychat.cafebazzartest.model.explore;


import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.GROUP_ITEM)
public class GroupItem implements Parcelable {
    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @Embedded(prefix = DbConstants.Prefixes.REASONS)
    @SerializedName("reasons")
    public Reasons reasons;

    @Embedded(prefix = DbConstants.Prefixes.VENUE)
    @SerializedName("venue")
    public Venue venue;


    @ColumnInfo(name = DbConstants.ColumnNames.GroupItem.TIPS)
    @SerializedName("tips")
    public List<Tips> tips;

    public final static Creator<GroupItem> CREATOR = new Creator<GroupItem>() {


        public GroupItem createFromParcel(android.os.Parcel in) {
            return new GroupItem(in);
        }

        public GroupItem[] newArray(int size) {
            return (new GroupItem[size]);
        }

    };

    protected GroupItem(android.os.Parcel in) {
        this.reasons = ((Reasons) in.readValue((Reasons.class.getClassLoader())));
        this.venue = ((Venue) in.readValue((Venue.class.getClassLoader())));
    }

    public GroupItem() {
    }

    public Reasons getReasons() {
        return reasons;
    }

    public void setReasons(Reasons reasons) {
        this.reasons = reasons;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public List<Tips> getTips() {
        return tips;
    }

    public void setTips(List<Tips> tips) {
        this.tips = tips;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(GroupItem.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("reasons");
        sb.append('=');
        sb.append(((this.reasons == null) ? "<null>" : this.reasons));
        sb.append(',');
        sb.append("venue");
        sb.append('=');
        sb.append(((this.venue == null) ? "<null>" : this.venue));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.venue == null) ? 0 : this.venue.hashCode()));
        result = ((result * 31) + ((this.reasons == null) ? 0 : this.reasons.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof GroupItem) == false) {
            return false;
        }
        GroupItem rhs = ((GroupItem) other);
        return (((this.venue == rhs.venue) || ((this.venue != null) && this.venue.equals(rhs.venue))) && ((this.reasons == rhs.reasons) || ((this.reasons != null) && this.reasons.equals(rhs.reasons))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(reasons);
        dest.writeValue(venue);
    }

    public int describeContents() {
        return 0;
    }

}
