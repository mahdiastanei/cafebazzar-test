
package io.raychat.cafebazzartest.model.explore;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;
import io.raychat.cafebazzartest.utils.room.converters.GroupConverter;
import io.raychat.cafebazzartest.utils.room.converters.PhotoGroupConverter;

@Entity(tableName = DbConstants.TableNames.PHOTOS)
public class Photos {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Photos.COUNT)
    @SerializedName("count")
    public Integer count;

    @ColumnInfo(name = DbConstants.ColumnNames.Photos.GROUPS)
    @SerializedName("groups")
    @TypeConverters(PhotoGroupConverter.class)
    public List<PhotoGroup> groups = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<PhotoGroup> getGroups() {
        return groups;
    }

    public void setGroups(List<PhotoGroup> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Photos.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("count");
        sb.append('=');
        sb.append(((this.count == null)?"<null>":this.count));
        sb.append(',');
        sb.append("groups");
        sb.append('=');
        sb.append(((this.groups == null)?"<null>":this.groups));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.count == null)? 0 :this.count.hashCode()));
        result = ((result* 31)+((this.groups == null)? 0 :this.groups.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Photos) == false) {
            return false;
        }
        Photos rhs = ((Photos) other);
        return (((this.count == rhs.count)||((this.count!= null)&&this.count.equals(rhs.count)))&&((this.groups == rhs.groups)||((this.groups!= null)&&this.groups.equals(rhs.groups))));
    }

}
