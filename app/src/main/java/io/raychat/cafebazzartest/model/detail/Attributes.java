
package io.raychat.cafebazzartest.model.detail;

import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;


public class Attributes {

    @SerializedName("groups")
    private List<Group> groups = null;

    public List<Group> getGroups() {
        return groups;
    }

    public List<Group> getTipGroups() {
        if (groups != null && groups.size() > 0) {
            for (int i = 0; i < groups.size(); i++) {
                if (groups.get(i).getType().equals("others") || groups.get(i).getName().equals("All tips")) {
                    return Collections.singletonList(groups.get(i));
                }
            }
        }
        return groups;
    }


    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Attributes.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("groups");
        sb.append('=');
        sb.append(((this.groups == null) ? "<null>" : this.groups));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.groups == null) ? 0 : this.groups.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Attributes) == false) {
            return false;
        }
        Attributes rhs = ((Attributes) other);
        return ((this.groups == rhs.groups) || ((this.groups != null) && this.groups.equals(rhs.groups)));
    }

}
