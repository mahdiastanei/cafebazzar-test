
package io.raychat.cafebazzartest.model.detail;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Colors {

    @SerializedName("highlightColor")
    private HighlightColor highlightColor;
    @SerializedName("highlightTextColor")
    private HighlightTextColor highlightTextColor;
    @SerializedName("algoVersion")
    private Integer algoVersion;

    public HighlightColor getHighlightColor() {
        return highlightColor;
    }

    public void setHighlightColor(HighlightColor highlightColor) {
        this.highlightColor = highlightColor;
    }

    public HighlightTextColor getHighlightTextColor() {
        return highlightTextColor;
    }

    public void setHighlightTextColor(HighlightTextColor highlightTextColor) {
        this.highlightTextColor = highlightTextColor;
    }

    public Integer getAlgoVersion() {
        return algoVersion;
    }

    public void setAlgoVersion(Integer algoVersion) {
        this.algoVersion = algoVersion;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Colors.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("highlightColor");
        sb.append('=');
        sb.append(((this.highlightColor == null)?"<null>":this.highlightColor));
        sb.append(',');
        sb.append("highlightTextColor");
        sb.append('=');
        sb.append(((this.highlightTextColor == null)?"<null>":this.highlightTextColor));
        sb.append(',');
        sb.append("algoVersion");
        sb.append('=');
        sb.append(((this.algoVersion == null)?"<null>":this.algoVersion));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.highlightColor == null)? 0 :this.highlightColor.hashCode()));
        result = ((result* 31)+((this.algoVersion == null)? 0 :this.algoVersion.hashCode()));
        result = ((result* 31)+((this.highlightTextColor == null)? 0 :this.highlightTextColor.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Colors) == false) {
            return false;
        }
        Colors rhs = ((Colors) other);
        return ((((this.highlightColor == rhs.highlightColor)||((this.highlightColor!= null)&&this.highlightColor.equals(rhs.highlightColor)))&&((this.algoVersion == rhs.algoVersion)||((this.algoVersion!= null)&&this.algoVersion.equals(rhs.algoVersion))))&&((this.highlightTextColor == rhs.highlightTextColor)||((this.highlightTextColor!= null)&&this.highlightTextColor.equals(rhs.highlightTextColor))));
    }

}
