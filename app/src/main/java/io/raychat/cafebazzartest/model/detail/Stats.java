
package io.raychat.cafebazzartest.model.detail;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

@Entity(tableName = DbConstants.TableNames.STATS)
public class Stats {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Stats.TIP_COUNT)
    @SerializedName("tipCount")
    public Integer tipCount;

    @ColumnInfo(name = DbConstants.ColumnNames.Stats.USERS_COUNT)
    @SerializedName("usersCount")
    public Integer usersCount;

    @ColumnInfo(name = DbConstants.ColumnNames.Stats.CHECKINS_COUNT)
    @SerializedName("checkinsCount")
    public Integer checkinsCount = 0;

    @ColumnInfo(name = DbConstants.ColumnNames.Stats.VISITS_COUNT)
    @SerializedName("visitsCount")
    public Integer visitsCount;

    public String getTipCount() {
        return String.valueOf(tipCount);
    }

    public void setTipCount(Integer tipCount) {
        this.tipCount = tipCount;
    }

    public String getUsersCount() {
        return String.valueOf(usersCount);
    }

    public void setUsersCount(Integer usersCount) {
        this.usersCount = usersCount;
    }

    public String getCheckinsCount() {
        return String.valueOf(checkinsCount);
    }

    public void setCheckinsCount(Integer checkinsCount) {
        this.checkinsCount = checkinsCount;
    }

    public Integer getVisitsCount() {
        return visitsCount;
    }

    public void setVisitsCount(Integer visitsCount) {
        this.visitsCount = visitsCount;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Stats.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("tipCount");
        sb.append('=');
        sb.append(((this.tipCount == null) ? "<null>" : this.tipCount));
        sb.append(',');
        sb.append("usersCount");
        sb.append('=');
        sb.append(((this.usersCount == null) ? "<null>" : this.usersCount));
        sb.append(',');
        sb.append("checkinsCount");
        sb.append('=');
        sb.append(((this.checkinsCount == null) ? "<null>" : this.checkinsCount));
        sb.append(',');
        sb.append("visitsCount");
        sb.append('=');
        sb.append(((this.visitsCount == null) ? "<null>" : this.visitsCount));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.visitsCount == null) ? 0 : this.visitsCount.hashCode()));
        result = ((result * 31) + ((this.usersCount == null) ? 0 : this.usersCount.hashCode()));
        result = ((result * 31) + ((this.tipCount == null) ? 0 : this.tipCount.hashCode()));
        result = ((result * 31) + ((this.checkinsCount == null) ? 0 : this.checkinsCount.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Stats) == false) {
            return false;
        }
        Stats rhs = ((Stats) other);
        return (((((this.visitsCount == rhs.visitsCount) || ((this.visitsCount != null) && this.visitsCount.equals(rhs.visitsCount))) && ((this.usersCount == rhs.usersCount) || ((this.usersCount != null) && this.usersCount.equals(rhs.usersCount)))) && ((this.tipCount == rhs.tipCount) || ((this.tipCount != null) && this.tipCount.equals(rhs.tipCount)))) && ((this.checkinsCount == rhs.checkinsCount) || ((this.checkinsCount != null) && this.checkinsCount.equals(rhs.checkinsCount))));
    }

}
