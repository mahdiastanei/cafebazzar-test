
package io.raychat.cafebazzartest.model.explore;


import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.ITEM)
public class Item implements Parcelable {
    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Item.SUMMARY)
    @SerializedName("summary")
    public String summary;

    @ColumnInfo(name = DbConstants.ColumnNames.Item.TYPE)
    @SerializedName("type")
    public String type;

    @ColumnInfo(name = DbConstants.ColumnNames.Item.REASON_NAME)
    @SerializedName("reasonName")
    public String reasonName;

    public final static Creator<Item> CREATOR = new Creator<Item>() {


        public Item createFromParcel(android.os.Parcel in) {
            return new Item(in);
        }

        public Item[] newArray(int size) {
            return (new Item[size]);
        }

    };

    protected Item(android.os.Parcel in) {
        this.summary = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.reasonName = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Item() {
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Item.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("summary");
        sb.append('=');
        sb.append(((this.summary == null) ? "<null>" : this.summary));
        sb.append(',');
        sb.append("type");
        sb.append('=');
        sb.append(((this.type == null) ? "<null>" : this.type));
        sb.append(',');
        sb.append("reasonName");
        sb.append('=');
        sb.append(((this.reasonName == null) ? "<null>" : this.reasonName));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.summary == null) ? 0 : this.summary.hashCode()));
        result = ((result * 31) + ((this.reasonName == null) ? 0 : this.reasonName.hashCode()));
        result = ((result * 31) + ((this.type == null) ? 0 : this.type.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Item) == false) {
            return false;
        }
        Item rhs = ((Item) other);
        return ((((this.summary == rhs.summary) || ((this.summary != null) && this.summary.equals(rhs.summary))) && ((this.reasonName == rhs.reasonName) || ((this.reasonName != null) && this.reasonName.equals(rhs.reasonName)))) && ((this.type == rhs.type) || ((this.type != null) && this.type.equals(rhs.type))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(summary);
        dest.writeValue(type);
        dest.writeValue(reasonName);
    }

    public int describeContents() {
        return 0;
    }

}
