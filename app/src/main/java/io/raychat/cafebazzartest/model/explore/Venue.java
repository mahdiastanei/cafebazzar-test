
package io.raychat.cafebazzartest.model.explore;

import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.raychat.cafebazzartest.model.detail.Attributes;
import io.raychat.cafebazzartest.model.detail.Stats;
import io.raychat.cafebazzartest.utils.constants.DbConstants;
import io.raychat.cafebazzartest.utils.room.converters.CategoryConverter;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.VENUE)
public class Venue implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Venue.ID)
    @SerializedName("id")
    public String id;

    @ColumnInfo(name = DbConstants.ColumnNames.Venue.NAME)
    @SerializedName("name")
    public String name = "";

    @Embedded(prefix = DbConstants.Prefixes.LOCATION)
    @SerializedName("location")
    public Location location;

    @ColumnInfo(name = DbConstants.ColumnNames.Venue.CATEGORIES)
    @TypeConverters(CategoryConverter.class)
    @SerializedName("categories")
    public List<Category> categories = null;

    @ColumnInfo(name = DbConstants.ColumnNames.Venue.POPULARITY_BY_GEO)
    @SerializedName("popularityByGeo")
    public Double popularityByGeo;

    @ColumnInfo(name = DbConstants.ColumnNames.Venue.VERIFIED)
    @SerializedName("verified")
    public boolean verified;

    @ColumnInfo(name = DbConstants.ColumnNames.Venue.RATING)
    @SerializedName("rating")
    public String rating;

    @ColumnInfo(name = DbConstants.ColumnNames.Venue.RATING_COLOR)
    @SerializedName("ratingColor")
    public String ratingColor;

    @ColumnInfo(name = DbConstants.ColumnNames.Venue.RATING_SIGNALS)
    @SerializedName("ratingSignals")
    public String ratingSignals;

    @Embedded(prefix = DbConstants.Prefixes.VENUE_PAGE)
    @SerializedName("venuePage")
    public VenuePage venuePage;

    @Embedded(prefix = DbConstants.Prefixes.PHOTOS)
    @SerializedName("photos")
    public Photos photos;

    @Embedded(prefix = DbConstants.Prefixes.STATS)
    @SerializedName("stats")
    public Stats stats;

    @Embedded(prefix = DbConstants.Prefixes.HOURS)
    @SerializedName("hours")
    public Hours hours;

    @Ignore
    @SerializedName("tips")
    public Attributes tips;


    public final static Creator<Venue> CREATOR = new Creator<Venue>() {


        public Venue createFromParcel(android.os.Parcel in) {
            return new Venue(in);
        }

        public Venue[] newArray(int size) {
            return (new Venue[size]);
        }

    };

    protected Venue(android.os.Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.location = ((Location) in.readValue((Location.class.getClassLoader())));
        in.readList(this.categories, (io.raychat.cafebazzartest.model.explore.Category.class.getClassLoader()));
        this.popularityByGeo = ((Double) in.readValue((Double.class.getClassLoader())));
        this.venuePage = ((VenuePage) in.readValue((VenuePage.class.getClassLoader())));
    }

    public Venue() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Double getPopularityByGeo() {
        return popularityByGeo;
    }

    public void setPopularityByGeo(Double popularityByGeo) {
        this.popularityByGeo = popularityByGeo;
    }

    public VenuePage getVenuePage() {
        return venuePage;
    }

    public void setVenuePage(VenuePage venuePage) {
        this.venuePage = venuePage;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public Hours getHours() {
        return hours;
    }

    public void setHours(Hours hours) {
        this.hours = hours;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRatingColor() {
        return ratingColor;
    }

    public void setRatingColor(String ratingColor) {
        this.ratingColor = ratingColor;
    }

    public String getRatingSignals() {
        return ratingSignals;
    }

    public void setRatingSignals(String ratingSignals) {
        this.ratingSignals = ratingSignals;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Venue.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null) ? "<null>" : this.id));
        sb.append(',');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null) ? "<null>" : this.name));
        sb.append(',');
        sb.append("location");
        sb.append('=');
        sb.append(((this.location == null) ? "<null>" : this.location));
        sb.append(',');
        sb.append("categories");
        sb.append('=');
        sb.append(((this.categories == null) ? "<null>" : this.categories));
        sb.append(',');
        sb.append("popularityByGeo");
        sb.append('=');
        sb.append(((this.popularityByGeo == null) ? "<null>" : this.popularityByGeo));
        sb.append(',');
        sb.append("venuePage");
        sb.append('=');
        sb.append(((this.venuePage == null) ? "<null>" : this.venuePage));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.popularityByGeo == null) ? 0 : this.popularityByGeo.hashCode()));
        result = ((result * 31) + ((this.venuePage == null) ? 0 : this.venuePage.hashCode()));
        result = ((result * 31) + ((this.name == null) ? 0 : this.name.hashCode()));
        result = ((result * 31) + ((this.location == null) ? 0 : this.location.hashCode()));
        result = ((result * 31) + ((this.id == null) ? 0 : this.id.hashCode()));
        result = ((result * 31) + ((this.categories == null) ? 0 : this.categories.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Venue) == false) {
            return false;
        }
        Venue rhs = ((Venue) other);
        return (((((((this.popularityByGeo == rhs.popularityByGeo) || ((this.popularityByGeo != null) && this.popularityByGeo.equals(rhs.popularityByGeo))) && ((this.venuePage == rhs.venuePage) || ((this.venuePage != null) && this.venuePage.equals(rhs.venuePage)))) && ((this.name == rhs.name) || ((this.name != null) && this.name.equals(rhs.name)))) && ((this.location == rhs.location) || ((this.location != null) && this.location.equals(rhs.location)))) && ((this.id == rhs.id) || ((this.id != null) && this.id.equals(rhs.id)))) && ((this.categories == rhs.categories) || ((this.categories != null) && this.categories.equals(rhs.categories))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(location);
        dest.writeList(categories);
        dest.writeValue(popularityByGeo);
        dest.writeValue(venuePage);
    }

    public int describeContents() {
        return 0;
    }

}
