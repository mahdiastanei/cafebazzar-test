package io.raychat.cafebazzartest.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/6/2021.
 */

@Entity(tableName = DbConstants.TableNames.MY_LOCATION)
public class MyLocation implements Parcelable {

    @ColumnInfo(name = DbConstants.ColumnNames.MyLocation.LAT)
    public double latitude;

    @ColumnInfo(name = DbConstants.ColumnNames.MyLocation.LNG)
    public double longitude;

    @ColumnInfo(name = DbConstants.ColumnNames.MyLocation.DISTANCE)
    public double distance;

    @ColumnInfo(name = DbConstants.ColumnNames.MyLocation.SHOULD_REFRESH)
    public boolean shouldRefresh;

    @PrimaryKey(autoGenerate = false)
    public int id = 0;

    public MyLocation() {
    }

    protected MyLocation(Parcel in) {
        latitude = in.readDouble();
        longitude = in.readDouble();
        distance = in.readDouble();
        shouldRefresh = in.readByte() != 0;
    }

    public static final Creator<MyLocation> CREATOR = new Creator<MyLocation>() {
        @Override
        public MyLocation createFromParcel(Parcel in) {
            return new MyLocation(in);
        }

        @Override
        public MyLocation[] newArray(int size) {
            return new MyLocation[size];
        }
    };

    public MyLocation setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public MyLocation setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public MyLocation setDistance(double distance) {
        this.distance = distance;
        return this;
    }

    public MyLocation setShouldRefresh(boolean shouldRefresh) {
        this.shouldRefresh = shouldRefresh;
        return this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getDistance() {
        return distance;
    }

    public boolean shouldRefresh() {
        return shouldRefresh;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeDouble(distance);
        dest.writeByte((byte) (shouldRefresh ? 1 : 0));
    }

    public String getLocation() {
        return latitude + "," + longitude;
    }
}
