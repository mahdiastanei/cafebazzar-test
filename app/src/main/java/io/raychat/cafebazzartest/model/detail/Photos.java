
package io.raychat.cafebazzartest.model.detail;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.raychat.cafebazzartest.model.explore.PhotoGroup;

public class Photos {

    @SerializedName("count")
    private Integer count;
    @SerializedName("groups")
    private List<Group> groups = null;
    @SerializedName("items")
    private List<PhotoGroup> items = null;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public Group getVenuePhotos() {
        if (groups != null) {
            for (int i = 0; i < groups.size(); i++) {
                if (groups.get(i).getName().equals("Venue photos") || groups.get(i).getType().equals("venue")) {
                    return groups.get(i);
                }
            }
        }
        return null;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<PhotoGroup> getItems() {
        return items;
    }

    public void setItems(List<PhotoGroup> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Photos.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("count");
        sb.append('=');
        sb.append(((this.count == null) ? "<null>" : this.count));
        sb.append(',');
        sb.append("groups");
        sb.append('=');
        sb.append(((this.groups == null) ? "<null>" : this.groups));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.count == null) ? 0 : this.count.hashCode()));
        result = ((result * 31) + ((this.groups == null) ? 0 : this.groups.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Photos) == false) {
            return false;
        }
        Photos rhs = ((Photos) other);
        return (((this.count == rhs.count) || ((this.count != null) && this.count.equals(rhs.count))) && ((this.groups == rhs.groups) || ((this.groups != null) && this.groups.equals(rhs.groups))));
    }

}
