
package io.raychat.cafebazzartest.model.detail;


import com.google.gson.annotations.SerializedName;


public class Response {

    @SerializedName("venue")
    private Venue venue;

    @SerializedName("photos")
    private Photos photos;

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Response.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("venue");
        sb.append('=');
        sb.append(((this.venue == null)?"<null>":this.venue));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.venue == null)? 0 :this.venue.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Response) == false) {
            return false;
        }
        Response rhs = ((Response) other);
        return ((this.venue == rhs.venue)||((this.venue!= null)&&this.venue.equals(rhs.venue)));
    }

}
