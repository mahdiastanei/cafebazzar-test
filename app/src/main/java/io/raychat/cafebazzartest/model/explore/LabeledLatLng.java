
package io.raychat.cafebazzartest.model.explore;


import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.LABELED_LAT_LNG)
public class LabeledLatLng implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.LabeledLatLng.LABEL)
    @SerializedName("label")
    public String label;

    @ColumnInfo(name = DbConstants.ColumnNames.LabeledLatLng.LAT)
    @SerializedName("lat")
    public Double lat;

    @ColumnInfo(name = DbConstants.ColumnNames.LabeledLatLng.LNG)
    @SerializedName("lng")
    public Double lng;

    public final static Creator<LabeledLatLng> CREATOR = new Creator<LabeledLatLng>() {


        public LabeledLatLng createFromParcel(android.os.Parcel in) {
            return new LabeledLatLng(in);
        }

        public LabeledLatLng[] newArray(int size) {
            return (new LabeledLatLng[size]);
        }

    };

    protected LabeledLatLng(android.os.Parcel in) {
        this.label = ((String) in.readValue((String.class.getClassLoader())));
        this.lat = ((Double) in.readValue((Double.class.getClassLoader())));
        this.lng = ((Double) in.readValue((Double.class.getClassLoader())));
    }

    public LabeledLatLng() {
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(LabeledLatLng.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("label");
        sb.append('=');
        sb.append(((this.label == null) ? "<null>" : this.label));
        sb.append(',');
        sb.append("lat");
        sb.append('=');
        sb.append(((this.lat == null) ? "<null>" : this.lat));
        sb.append(',');
        sb.append("lng");
        sb.append('=');
        sb.append(((this.lng == null) ? "<null>" : this.lng));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.label == null) ? 0 : this.label.hashCode()));
        result = ((result * 31) + ((this.lng == null) ? 0 : this.lng.hashCode()));
        result = ((result * 31) + ((this.lat == null) ? 0 : this.lat.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof LabeledLatLng) == false) {
            return false;
        }
        LabeledLatLng rhs = ((LabeledLatLng) other);
        return ((((this.label == rhs.label) || ((this.label != null) && this.label.equals(rhs.label))) && ((this.lng == rhs.lng) || ((this.lng != null) && this.lng.equals(rhs.lng)))) && ((this.lat == rhs.lat) || ((this.lat != null) && this.lat.equals(rhs.lat))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(label);
        dest.writeValue(lat);
        dest.writeValue(lng);
    }

    public int describeContents() {
        return 0;
    }

}
