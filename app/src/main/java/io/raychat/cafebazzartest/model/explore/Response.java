
package io.raychat.cafebazzartest.model.explore;

import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.raychat.cafebazzartest.model.detail.Photos;
import io.raychat.cafebazzartest.utils.constants.DbConstants;
import io.raychat.cafebazzartest.utils.room.converters.GroupConverter;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.RESPONSE)
public class Response implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @Embedded(prefix = DbConstants.Prefixes.WARNING)
    @SerializedName("warning")
    public Warning warning;

    @ColumnInfo(name = DbConstants.ColumnNames.Response.SUGGESTED_RADIUS)
    @SerializedName("suggestedRadius")
    public Integer suggestedRadius;

    @ColumnInfo(name = DbConstants.ColumnNames.Response.HEADER_LOCATION)
    @SerializedName("headerLocation")
    public String headerLocation;

    @ColumnInfo(name = DbConstants.ColumnNames.Response.HEADER_FULL_LOCATION)
    @SerializedName("headerFullLocation")
    public String headerFullLocation;

    @ColumnInfo(name = DbConstants.ColumnNames.Response.HEADER_LOCATION_GRANULARITY)
    @SerializedName("headerLocationGranularity")
    public String headerLocationGranularity;

    @ColumnInfo(name = DbConstants.ColumnNames.Response.TOTAL_RESULTS)
    @SerializedName("totalResults")
    public Integer totalResults;

    @Embedded(prefix = DbConstants.Prefixes.SUGGESTED_BOUNDS)
    @SerializedName("suggestedBounds")
    public SuggestedBounds suggestedBounds;

    @ColumnInfo(name = DbConstants.ColumnNames.Response.GROUPS)
    @TypeConverters(GroupConverter.class)
    @SerializedName("groups")
    public List<Group> groups = null;

    @Embedded(prefix = DbConstants.Prefixes.VENUE)
    @SerializedName("venue")
    private Venue venue;

    @Ignore
    @SerializedName("photos")
    public Photos photos;

    public final static Creator<Response> CREATOR = new Creator<Response>() {


        public Response createFromParcel(android.os.Parcel in) {
            return new Response(in);
        }

        public Response[] newArray(int size) {
            return (new Response[size]);
        }

    };

    protected Response(android.os.Parcel in) {
        this.warning = ((Warning) in.readValue((Warning.class.getClassLoader())));
        this.suggestedRadius = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.headerLocation = ((String) in.readValue((String.class.getClassLoader())));
        this.headerFullLocation = ((String) in.readValue((String.class.getClassLoader())));
        this.headerLocationGranularity = ((String) in.readValue((String.class.getClassLoader())));
        this.totalResults = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.suggestedBounds = ((SuggestedBounds) in.readValue((SuggestedBounds.class.getClassLoader())));
        in.readList(this.groups, (io.raychat.cafebazzartest.model.explore.Group.class.getClassLoader()));
    }

    public Response() {
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

    public Warning getWarning() {
        return warning;
    }

    public void setWarning(Warning warning) {
        this.warning = warning;
    }

    public Integer getSuggestedRadius() {
        return suggestedRadius;
    }

    public void setSuggestedRadius(Integer suggestedRadius) {
        this.suggestedRadius = suggestedRadius;
    }

    public String getHeaderLocation() {
        return headerLocation;
    }

    public void setHeaderLocation(String headerLocation) {
        this.headerLocation = headerLocation;
    }

    public String getHeaderFullLocation() {
        return headerFullLocation;
    }

    public void setHeaderFullLocation(String headerFullLocation) {
        this.headerFullLocation = headerFullLocation;
    }

    public String getHeaderLocationGranularity() {
        return headerLocationGranularity;
    }

    public void setHeaderLocationGranularity(String headerLocationGranularity) {
        this.headerLocationGranularity = headerLocationGranularity;
    }

    public Integer getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Integer totalResults) {
        this.totalResults = totalResults;
    }

    public SuggestedBounds getSuggestedBounds() {
        return suggestedBounds;
    }

    public void setSuggestedBounds(SuggestedBounds suggestedBounds) {
        this.suggestedBounds = suggestedBounds;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public Group getRecommendedGroups() {
        for (int i = 0; i < groups.size(); i++) {
            if (groups.get(i).getType().equals("Recommended Places") || groups.get(i).getName().equals("recommended")) {
                return groups.get(i);
            }
        }
        return groups.get(0);
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Response.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("warning");
        sb.append('=');
        sb.append(((this.warning == null) ? "<null>" : this.warning));
        sb.append(',');
        sb.append("suggestedRadius");
        sb.append('=');
        sb.append(((this.suggestedRadius == null) ? "<null>" : this.suggestedRadius));
        sb.append(',');
        sb.append("headerLocation");
        sb.append('=');
        sb.append(((this.headerLocation == null) ? "<null>" : this.headerLocation));
        sb.append(',');
        sb.append("headerFullLocation");
        sb.append('=');
        sb.append(((this.headerFullLocation == null) ? "<null>" : this.headerFullLocation));
        sb.append(',');
        sb.append("headerLocationGranularity");
        sb.append('=');
        sb.append(((this.headerLocationGranularity == null) ? "<null>" : this.headerLocationGranularity));
        sb.append(',');
        sb.append("totalResults");
        sb.append('=');
        sb.append(((this.totalResults == null) ? "<null>" : this.totalResults));
        sb.append(',');
        sb.append("suggestedBounds");
        sb.append('=');
        sb.append(((this.suggestedBounds == null) ? "<null>" : this.suggestedBounds));
        sb.append(',');
        sb.append("groups");
        sb.append('=');
        sb.append(((this.groups == null) ? "<null>" : this.groups));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.totalResults == null) ? 0 : this.totalResults.hashCode()));
        result = ((result * 31) + ((this.suggestedRadius == null) ? 0 : this.suggestedRadius.hashCode()));
        result = ((result * 31) + ((this.headerFullLocation == null) ? 0 : this.headerFullLocation.hashCode()));
        result = ((result * 31) + ((this.warning == null) ? 0 : this.warning.hashCode()));
        result = ((result * 31) + ((this.headerLocationGranularity == null) ? 0 : this.headerLocationGranularity.hashCode()));
        result = ((result * 31) + ((this.groups == null) ? 0 : this.groups.hashCode()));
        result = ((result * 31) + ((this.suggestedBounds == null) ? 0 : this.suggestedBounds.hashCode()));
        result = ((result * 31) + ((this.headerLocation == null) ? 0 : this.headerLocation.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Response) == false) {
            return false;
        }
        Response rhs = ((Response) other);
        return (((((((((this.totalResults == rhs.totalResults) || ((this.totalResults != null) && this.totalResults.equals(rhs.totalResults))) && ((this.suggestedRadius == rhs.suggestedRadius) || ((this.suggestedRadius != null) && this.suggestedRadius.equals(rhs.suggestedRadius)))) && ((this.headerFullLocation == rhs.headerFullLocation) || ((this.headerFullLocation != null) && this.headerFullLocation.equals(rhs.headerFullLocation)))) && ((this.warning == rhs.warning) || ((this.warning != null) && this.warning.equals(rhs.warning)))) && ((this.headerLocationGranularity == rhs.headerLocationGranularity) || ((this.headerLocationGranularity != null) && this.headerLocationGranularity.equals(rhs.headerLocationGranularity)))) && ((this.groups == rhs.groups) || ((this.groups != null) && this.groups.equals(rhs.groups)))) && ((this.suggestedBounds == rhs.suggestedBounds) || ((this.suggestedBounds != null) && this.suggestedBounds.equals(rhs.suggestedBounds)))) && ((this.headerLocation == rhs.headerLocation) || ((this.headerLocation != null) && this.headerLocation.equals(rhs.headerLocation))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(warning);
        dest.writeValue(suggestedRadius);
        dest.writeValue(headerLocation);
        dest.writeValue(headerFullLocation);
        dest.writeValue(headerLocationGranularity);
        dest.writeValue(totalResults);
        dest.writeValue(suggestedBounds);
        dest.writeList(groups);
    }

    public int describeContents() {
        return 0;
    }

}
