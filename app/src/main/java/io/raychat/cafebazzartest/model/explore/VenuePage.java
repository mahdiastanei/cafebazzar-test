
package io.raychat.cafebazzartest.model.explore;


import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.VENUE_PAGE)
public class VenuePage implements Parcelable {
    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.VenuePage.ID)
    @SerializedName("id")
    public String id;

    public final static Creator<VenuePage> CREATOR = new Creator<VenuePage>() {


        public VenuePage createFromParcel(android.os.Parcel in) {
            return new VenuePage(in);
        }

        public VenuePage[] newArray(int size) {
            return (new VenuePage[size]);
        }

    };

    protected VenuePage(android.os.Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
    }

    public VenuePage() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(VenuePage.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null) ? "<null>" : this.id));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.id == null) ? 0 : this.id.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof VenuePage) == false) {
            return false;
        }
        VenuePage rhs = ((VenuePage) other);
        return ((this.id == rhs.id) || ((this.id != null) && this.id.equals(rhs.id)));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
    }

    public int describeContents() {
        return 0;
    }

}
