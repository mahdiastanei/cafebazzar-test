
package io.raychat.cafebazzartest.model.detail;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.raychat.cafebazzartest.model.explore.Tips;


public class Venue {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("contact")
    private Contact contact;
    @SerializedName("location")
    private Location location;
    @SerializedName("canonicalUrl")
    private String canonicalUrl;
    @SerializedName("categories")
    private List<Category> categories = null;
    @SerializedName("stats")
    private Stats stats;
    @SerializedName("likes")
    private Likes likes;
    @SerializedName("dislike")
    private Boolean dislike;
    @SerializedName("ok")
    private Boolean ok;
    @SerializedName("specials")
    private Specials specials;
    @SerializedName("reasons")
    private Reasons reasons;
    @SerializedName("hereNow")
    private HereNow hereNow;
    @SerializedName("createdAt")
    private Integer createdAt;
    @SerializedName("verified")
    public boolean verified;
    @SerializedName("shortUrl")
    private String shortUrl;
    @SerializedName("timeZone")
    private String timeZone;
    @SerializedName("listed")
    private Listed listed;
    @SerializedName("seasonalHours")
    private List<Object> seasonalHours = null;
    @SerializedName("pageUpdates")
    private PageUpdates pageUpdates;
    @SerializedName("inbox")
    private Inbox inbox;
    @SerializedName("venueChains")
    private List<Object> venueChains = null;
    @SerializedName("attributes")
    private Attributes attributes;
    @SerializedName("bestPhoto")
    private BestPhoto bestPhoto;
    @SerializedName("photos")
    public Photos photos;
    @SerializedName("tips")
    public Attributes tips;
    @SerializedName("colors")
    private Colors colors;
    @SerializedName("rating")
    public String rating;
    @SerializedName("ratingColor")
    public String ratingColor;
    @SerializedName("ratingSignals")
    public Integer ratingSignals;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Location getLocation() {
        return location;
    }

    public Attributes getTips() {
        return tips;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getCanonicalUrl() {
        return canonicalUrl;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRatingColor() {
        return ratingColor;
    }

    public void setRatingColor(String ratingColor) {
        this.ratingColor = ratingColor;
    }

    public String getRatingSignals() {
        return String.valueOf(ratingSignals);
    }

    public void setRatingSignals(Integer ratingSignals) {
        this.ratingSignals = ratingSignals;
    }

    public void setCanonicalUrl(String canonicalUrl) {
        this.canonicalUrl = canonicalUrl;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public Likes getLikes() {
        return likes;
    }

    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    public Boolean getDislike() {
        return dislike;
    }

    public void setDislike(Boolean dislike) {
        this.dislike = dislike;
    }

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public Specials getSpecials() {
        return specials;
    }

    public void setSpecials(Specials specials) {
        this.specials = specials;
    }

    public Reasons getReasons() {
        return reasons;
    }

    public void setReasons(Reasons reasons) {
        this.reasons = reasons;
    }

    public HereNow getHereNow() {
        return hereNow;
    }

    public void setHereNow(HereNow hereNow) {
        this.hereNow = hereNow;
    }

    public Integer getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public Listed getListed() {
        return listed;
    }

    public void setListed(Listed listed) {
        this.listed = listed;
    }

    public List<Object> getSeasonalHours() {
        return seasonalHours;
    }

    public void setSeasonalHours(List<Object> seasonalHours) {
        this.seasonalHours = seasonalHours;
    }

    public PageUpdates getPageUpdates() {
        return pageUpdates;
    }

    public void setPageUpdates(PageUpdates pageUpdates) {
        this.pageUpdates = pageUpdates;
    }

    public Inbox getInbox() {
        return inbox;
    }

    public void setInbox(Inbox inbox) {
        this.inbox = inbox;
    }

    public List<Object> getVenueChains() {
        return venueChains;
    }

    public void setVenueChains(List<Object> venueChains) {
        this.venueChains = venueChains;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public BestPhoto getBestPhoto() {
        return bestPhoto;
    }

    public void setBestPhoto(BestPhoto bestPhoto) {
        this.bestPhoto = bestPhoto;
    }

    public Colors getColors() {
        return colors;
    }

    public void setColors(Colors colors) {
        this.colors = colors;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Venue.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null) ? "<null>" : this.id));
        sb.append(',');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null) ? "<null>" : this.name));
        sb.append(',');
        sb.append("contact");
        sb.append('=');
        sb.append(((this.contact == null) ? "<null>" : this.contact));
        sb.append(',');
        sb.append("location");
        sb.append('=');
        sb.append(((this.location == null) ? "<null>" : this.location));
        sb.append(',');
        sb.append("canonicalUrl");
        sb.append('=');
        sb.append(((this.canonicalUrl == null) ? "<null>" : this.canonicalUrl));
        sb.append(',');
        sb.append("categories");
        sb.append('=');
        sb.append(((this.categories == null) ? "<null>" : this.categories));
        sb.append(',');
        sb.append("stats");
        sb.append('=');
        sb.append(((this.stats == null) ? "<null>" : this.stats));
        sb.append(',');
        sb.append("likes");
        sb.append('=');
        sb.append(((this.likes == null) ? "<null>" : this.likes));
        sb.append(',');
        sb.append("dislike");
        sb.append('=');
        sb.append(((this.dislike == null) ? "<null>" : this.dislike));
        sb.append(',');
        sb.append("ok");
        sb.append('=');
        sb.append(((this.ok == null) ? "<null>" : this.ok));
        sb.append(',');
        sb.append("specials");
        sb.append('=');
        sb.append(((this.specials == null) ? "<null>" : this.specials));
        sb.append(',');
        sb.append("reasons");
        sb.append('=');
        sb.append(((this.reasons == null) ? "<null>" : this.reasons));
        sb.append(',');
        sb.append("hereNow");
        sb.append('=');
        sb.append(((this.hereNow == null) ? "<null>" : this.hereNow));
        sb.append(',');
        sb.append("createdAt");
        sb.append('=');
        sb.append(((this.createdAt == null) ? "<null>" : this.createdAt));
        sb.append(',');
        sb.append("shortUrl");
        sb.append('=');
        sb.append(((this.shortUrl == null) ? "<null>" : this.shortUrl));
        sb.append(',');
        sb.append("timeZone");
        sb.append('=');
        sb.append(((this.timeZone == null) ? "<null>" : this.timeZone));
        sb.append(',');
        sb.append("listed");
        sb.append('=');
        sb.append(((this.listed == null) ? "<null>" : this.listed));
        sb.append(',');
        sb.append("seasonalHours");
        sb.append('=');
        sb.append(((this.seasonalHours == null) ? "<null>" : this.seasonalHours));
        sb.append(',');
        sb.append("pageUpdates");
        sb.append('=');
        sb.append(((this.pageUpdates == null) ? "<null>" : this.pageUpdates));
        sb.append(',');
        sb.append("inbox");
        sb.append('=');
        sb.append(((this.inbox == null) ? "<null>" : this.inbox));
        sb.append(',');
        sb.append("venueChains");
        sb.append('=');
        sb.append(((this.venueChains == null) ? "<null>" : this.venueChains));
        sb.append(',');
        sb.append("attributes");
        sb.append('=');
        sb.append(((this.attributes == null) ? "<null>" : this.attributes));
        sb.append(',');
        sb.append("bestPhoto");
        sb.append('=');
        sb.append(((this.bestPhoto == null) ? "<null>" : this.bestPhoto));
        sb.append(',');
        sb.append("colors");
        sb.append('=');
        sb.append(((this.colors == null) ? "<null>" : this.colors));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.reasons == null) ? 0 : this.reasons.hashCode()));
        result = ((result * 31) + ((this.specials == null) ? 0 : this.specials.hashCode()));
        result = ((result * 31) + ((this.dislike == null) ? 0 : this.dislike.hashCode()));
        result = ((result * 31) + ((this.shortUrl == null) ? 0 : this.shortUrl.hashCode()));
        result = ((result * 31) + ((this.colors == null) ? 0 : this.colors.hashCode()));
        result = ((result * 31) + ((this.hereNow == null) ? 0 : this.hereNow.hashCode()));
        result = ((result * 31) + ((this.createdAt == null) ? 0 : this.createdAt.hashCode()));
        result = ((result * 31) + ((this.stats == null) ? 0 : this.stats.hashCode()));
        result = ((result * 31) + ((this.seasonalHours == null) ? 0 : this.seasonalHours.hashCode()));
        result = ((result * 31) + ((this.contact == null) ? 0 : this.contact.hashCode()));
        result = ((result * 31) + ((this.id == null) ? 0 : this.id.hashCode()));
        result = ((result * 31) + ((this.categories == null) ? 0 : this.categories.hashCode()));
        result = ((result * 31) + ((this.ok == null) ? 0 : this.ok.hashCode()));
        result = ((result * 31) + ((this.likes == null) ? 0 : this.likes.hashCode()));
        result = ((result * 31) + ((this.canonicalUrl == null) ? 0 : this.canonicalUrl.hashCode()));
        result = ((result * 31) + ((this.venueChains == null) ? 0 : this.venueChains.hashCode()));
        result = ((result * 31) + ((this.timeZone == null) ? 0 : this.timeZone.hashCode()));
        result = ((result * 31) + ((this.bestPhoto == null) ? 0 : this.bestPhoto.hashCode()));
        result = ((result * 31) + ((this.listed == null) ? 0 : this.listed.hashCode()));
        result = ((result * 31) + ((this.name == null) ? 0 : this.name.hashCode()));
        result = ((result * 31) + ((this.location == null) ? 0 : this.location.hashCode()));
        result = ((result * 31) + ((this.attributes == null) ? 0 : this.attributes.hashCode()));
        result = ((result * 31) + ((this.pageUpdates == null) ? 0 : this.pageUpdates.hashCode()));
        result = ((result * 31) + ((this.inbox == null) ? 0 : this.inbox.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Venue) == false) {
            return false;
        }
        Venue rhs = ((Venue) other);
        return (((((((((((((((((((((((((this.reasons == rhs.reasons) || ((this.reasons != null) && this.reasons.equals(rhs.reasons))) && ((this.specials == rhs.specials) || ((this.specials != null) && this.specials.equals(rhs.specials)))) && ((this.dislike == rhs.dislike) || ((this.dislike != null) && this.dislike.equals(rhs.dislike)))) && ((this.shortUrl == rhs.shortUrl) || ((this.shortUrl != null) && this.shortUrl.equals(rhs.shortUrl)))) && ((this.colors == rhs.colors) || ((this.colors != null) && this.colors.equals(rhs.colors)))) && ((this.hereNow == rhs.hereNow) || ((this.hereNow != null) && this.hereNow.equals(rhs.hereNow)))) && ((this.createdAt == rhs.createdAt) || ((this.createdAt != null) && this.createdAt.equals(rhs.createdAt)))) && ((this.stats == rhs.stats) || ((this.stats != null) && this.stats.equals(rhs.stats)))) && ((this.seasonalHours == rhs.seasonalHours) || ((this.seasonalHours != null) && this.seasonalHours.equals(rhs.seasonalHours)))) && ((this.contact == rhs.contact) || ((this.contact != null) && this.contact.equals(rhs.contact)))) && ((this.id == rhs.id) || ((this.id != null) && this.id.equals(rhs.id)))) && ((this.categories == rhs.categories) || ((this.categories != null) && this.categories.equals(rhs.categories)))) && ((this.ok == rhs.ok) || ((this.ok != null) && this.ok.equals(rhs.ok)))) && ((this.likes == rhs.likes) || ((this.likes != null) && this.likes.equals(rhs.likes)))) && ((this.canonicalUrl == rhs.canonicalUrl) || ((this.canonicalUrl != null) && this.canonicalUrl.equals(rhs.canonicalUrl)))) && ((this.venueChains == rhs.venueChains) || ((this.venueChains != null) && this.venueChains.equals(rhs.venueChains)))) && ((this.timeZone == rhs.timeZone) || ((this.timeZone != null) && this.timeZone.equals(rhs.timeZone)))) && ((this.bestPhoto == rhs.bestPhoto) || ((this.bestPhoto != null) && this.bestPhoto.equals(rhs.bestPhoto)))) && ((this.listed == rhs.listed) || ((this.listed != null) && this.listed.equals(rhs.listed)))) && ((this.name == rhs.name) || ((this.name != null) && this.name.equals(rhs.name)))) && ((this.location == rhs.location) || ((this.location != null) && this.location.equals(rhs.location)))) && ((this.attributes == rhs.attributes) || ((this.attributes != null) && this.attributes.equals(rhs.attributes)))) && ((this.pageUpdates == rhs.pageUpdates) || ((this.pageUpdates != null) && this.pageUpdates.equals(rhs.pageUpdates)))) && ((this.inbox == rhs.inbox) || ((this.inbox != null) && this.inbox.equals(rhs.inbox))));
    }

}
