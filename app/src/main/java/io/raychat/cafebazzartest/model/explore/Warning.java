
package io.raychat.cafebazzartest.model.explore;


import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.WARNING)
public class Warning implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Warning.TEXT)
    @SerializedName("text")
    public String text;

    public final static Creator<Warning> CREATOR = new Creator<Warning>() {


        public Warning createFromParcel(android.os.Parcel in) {
            return new Warning(in);
        }

        public Warning[] newArray(int size) {
            return (new Warning[size]);
        }

    };

    protected Warning(android.os.Parcel in) {
        this.text = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Warning() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Warning.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("text");
        sb.append('=');
        sb.append(((this.text == null) ? "<null>" : this.text));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.text == null) ? 0 : this.text.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Warning) == false) {
            return false;
        }
        Warning rhs = ((Warning) other);
        return ((this.text == rhs.text) || ((this.text != null) && this.text.equals(rhs.text)));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(text);
    }

    public int describeContents() {
        return 0;
    }

}
