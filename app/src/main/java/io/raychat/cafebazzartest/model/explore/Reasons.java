
package io.raychat.cafebazzartest.model.explore;

import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.raychat.cafebazzartest.utils.constants.DbConstants;
import io.raychat.cafebazzartest.utils.room.converters.ItemConverter;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.REASONS)
public class Reasons implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Reasons.COUNT)
    @SerializedName("count")
    public Integer count;

    @ColumnInfo(name = DbConstants.ColumnNames.Reasons.ITEMS)
    @TypeConverters(ItemConverter.class)
    @SerializedName("items")
    public List<Item> items = null;

    public final static Creator<Reasons> CREATOR = new Creator<Reasons>() {


        public Reasons createFromParcel(android.os.Parcel in) {
            return new Reasons(in);
        }

        public Reasons[] newArray(int size) {
            return (new Reasons[size]);
        }

    };

    protected Reasons(android.os.Parcel in) {
        this.count = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.items, (Item.class.getClassLoader()));
    }


    public Reasons() {
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Reasons.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("count");
        sb.append('=');
        sb.append(((this.count == null) ? "<null>" : this.count));
        sb.append(',');
        sb.append("items");
        sb.append('=');
        sb.append(((this.items == null) ? "<null>" : this.items));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.count == null) ? 0 : this.count.hashCode()));
        result = ((result * 31) + ((this.items == null) ? 0 : this.items.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Reasons) == false) {
            return false;
        }
        Reasons rhs = ((Reasons) other);
        return (((this.count == rhs.count) || ((this.count != null) && this.count.equals(rhs.count))) && ((this.items == rhs.items) || ((this.items != null) && this.items.equals(rhs.items))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(count);
        dest.writeList(items);
    }

    public int describeContents() {
        return 0;
    }

}
