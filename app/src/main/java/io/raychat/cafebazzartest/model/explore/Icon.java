
package io.raychat.cafebazzartest.model.explore;


import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.ICON)
public class Icon implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Icon.PREFIX)
    @SerializedName("prefix")
    public String prefix;

    @ColumnInfo(name = DbConstants.ColumnNames.Icon.SUFFIX)
    @SerializedName("suffix")
    public String suffix;

    public final static Creator<Icon> CREATOR = new Creator<Icon>() {


        public Icon createFromParcel(android.os.Parcel in) {
            return new Icon(in);
        }

        public Icon[] newArray(int size) {
            return (new Icon[size]);
        }

    };

    protected Icon(android.os.Parcel in) {
        this.prefix = ((String) in.readValue((String.class.getClassLoader())));
        this.suffix = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Icon() {
    }


    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Icon.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("prefix");
        sb.append('=');
        sb.append(((this.prefix == null) ? "<null>" : this.prefix));
        sb.append(',');
        sb.append("suffix");
        sb.append('=');
        sb.append(((this.suffix == null) ? "<null>" : this.suffix));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.suffix == null) ? 0 : this.suffix.hashCode()));
        result = ((result * 31) + ((this.prefix == null) ? 0 : this.prefix.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Icon) == false) {
            return false;
        }
        Icon rhs = ((Icon) other);
        return (((this.suffix == rhs.suffix) || ((this.suffix != null) && this.suffix.equals(rhs.suffix))) && ((this.prefix == rhs.prefix) || ((this.prefix != null) && this.prefix.equals(rhs.prefix))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(prefix);
        dest.writeValue(suffix);
    }

    public int describeContents() {
        return 0;
    }

}
