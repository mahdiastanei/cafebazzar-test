
package io.raychat.cafebazzartest.model.explore;


import android.os.Parcelable;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.SUGGESTED_BOUNDS)
public class SuggestedBounds implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @Embedded(prefix = DbConstants.Prefixes.NE)
    @SerializedName("ne")
    public Ne ne;

    @Embedded(prefix = DbConstants.Prefixes.SW)
    @SerializedName("sw")
    public Sw sw;

    public final static Creator<SuggestedBounds> CREATOR = new Creator<SuggestedBounds>() {


        public SuggestedBounds createFromParcel(android.os.Parcel in) {
            return new SuggestedBounds(in);
        }

        public SuggestedBounds[] newArray(int size) {
            return (new SuggestedBounds[size]);
        }

    };

    protected SuggestedBounds(android.os.Parcel in) {
        this.ne = ((Ne) in.readValue((Ne.class.getClassLoader())));
        this.sw = ((Sw) in.readValue((Sw.class.getClassLoader())));
    }


    public SuggestedBounds() {
    }

    public Ne getNe() {
        return ne;
    }

    public void setNe(Ne ne) {
        this.ne = ne;
    }

    public Sw getSw() {
        return sw;
    }

    public void setSw(Sw sw) {
        this.sw = sw;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(SuggestedBounds.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("ne");
        sb.append('=');
        sb.append(((this.ne == null) ? "<null>" : this.ne));
        sb.append(',');
        sb.append("sw");
        sb.append('=');
        sb.append(((this.sw == null) ? "<null>" : this.sw));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.sw == null) ? 0 : this.sw.hashCode()));
        result = ((result * 31) + ((this.ne == null) ? 0 : this.ne.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SuggestedBounds) == false) {
            return false;
        }
        SuggestedBounds rhs = ((SuggestedBounds) other);
        return (((this.sw == rhs.sw) || ((this.sw != null) && this.sw.equals(rhs.sw))) && ((this.ne == rhs.ne) || ((this.ne != null) && this.ne.equals(rhs.ne))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(ne);
        dest.writeValue(sw);
    }

    public int describeContents() {
        return 0;
    }

}
