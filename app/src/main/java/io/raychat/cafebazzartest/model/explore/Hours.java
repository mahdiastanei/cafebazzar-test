
package io.raychat.cafebazzartest.model.explore;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

@Entity(tableName = DbConstants.TableNames.HOURS)
public class Hours {

    @ColumnInfo(name = DbConstants.ColumnNames.Hours.STATUS)
    @SerializedName("status")
    public String status;

    @ColumnInfo(name = DbConstants.ColumnNames.Hours.IS_OPEN)
    @SerializedName("isOpen")
    public Boolean isOpen;

    @ColumnInfo(name = DbConstants.ColumnNames.Hours.IS_LOCAL_HOLIDAY)
    @SerializedName("isLocalHoliday")
    public Boolean isLocalHoliday;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public Boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(Boolean isOpen) {
        this.isOpen = isOpen;
    }

    public Boolean getIsLocalHoliday() {
        return isLocalHoliday;
    }

    public void setIsLocalHoliday(Boolean isLocalHoliday) {
        this.isLocalHoliday = isLocalHoliday;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Hours.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("status");
        sb.append('=');
        sb.append(((this.status == null)?"<null>":this.status));
        sb.append(',');
        sb.append("isOpen");
        sb.append('=');
        sb.append(((this.isOpen == null)?"<null>":this.isOpen));
        sb.append(',');
        sb.append("isLocalHoliday");
        sb.append('=');
        sb.append(((this.isLocalHoliday == null)?"<null>":this.isLocalHoliday));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.isOpen == null)? 0 :this.isOpen.hashCode()));
        result = ((result* 31)+((this.isLocalHoliday == null)? 0 :this.isLocalHoliday.hashCode()));
        result = ((result* 31)+((this.status == null)? 0 :this.status.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Hours) == false) {
            return false;
        }
        Hours rhs = ((Hours) other);
        return (((((this.isOpen == rhs.isOpen)||((this.isOpen!= null)&&this.isOpen.equals(rhs.isOpen))))&&((this.isLocalHoliday == rhs.isLocalHoliday)||((this.isLocalHoliday!= null)&&this.isLocalHoliday.equals(rhs.isLocalHoliday))))&&((this.status == rhs.status)||((this.status!= null)&&this.status.equals(rhs.status))));
    }

}
