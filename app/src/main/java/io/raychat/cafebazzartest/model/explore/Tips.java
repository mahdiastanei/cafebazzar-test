
package io.raychat.cafebazzartest.model.explore;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

@Entity(tableName = DbConstants.TableNames.TIPS)
public class Tips {
    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Tips.ID)
    @SerializedName("id")
    public String id;

    @ColumnInfo(name = DbConstants.ColumnNames.Tips.CREATED_AT)
    @SerializedName("createdAt")
    public Integer createdAt;

    @ColumnInfo(name = DbConstants.ColumnNames.Tips.TEXT)
    @SerializedName("text")
    public String text;

    @ColumnInfo(name = DbConstants.ColumnNames.Tips.TYPE)
    @SerializedName("type")
    public String type;

    @ColumnInfo(name = DbConstants.ColumnNames.Tips.CANONICAL_URL)
    @SerializedName("canonicalUrl")
    public String canonicalUrl;

    @Embedded(prefix = DbConstants.Prefixes.PHOTO)
    @SerializedName("photo")
    public PhotoGroup photo;

    @ColumnInfo(name = DbConstants.ColumnNames.Tips.PHOTO_URL)
    @SerializedName("photourl")
    public String photoUrl;

    @Ignore
    @SerializedName("likes")
    public Likes likes;

    @ColumnInfo(name = DbConstants.ColumnNames.Tips.LOG_VIEW)
    @SerializedName("logView")
    public Boolean logView;

    @ColumnInfo(name = DbConstants.ColumnNames.Tips.AGREE_COUNT)
    @SerializedName("agreeCount")
    public Integer agreeCount;

    @ColumnInfo(name = DbConstants.ColumnNames.Tips.DIS_AGREE_COUNT)
    @SerializedName("disagreeCount")
    public Integer disagreeCount;

    @Ignore
    @SerializedName("todo")
    public Todo todo;

    @Embedded(prefix = DbConstants.Prefixes.USER)
    @SerializedName("user")
    public User user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Integer createdAt) {
        this.createdAt = createdAt;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCanonicalUrl() {
        return canonicalUrl;
    }

    public void setCanonicalUrl(String canonicalUrl) {
        this.canonicalUrl = canonicalUrl;
    }

    public PhotoGroup getPhoto() {
        return photo;
    }

    public void setPhoto(PhotoGroup photo) {
        this.photo = photo;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Likes getLikes() {
        return likes;
    }

    public void setLikes(Likes likes) {
        this.likes = likes;
    }

    public Boolean getLogView() {
        return logView;
    }

    public void setLogView(Boolean logView) {
        this.logView = logView;
    }

    public Integer getAgreeCount() {
        return agreeCount;
    }

    public void setAgreeCount(Integer agreeCount) {
        this.agreeCount = agreeCount;
    }

    public Integer getDisagreeCount() {
        return disagreeCount;
    }

    public void setDisagreeCount(Integer disagreeCount) {
        this.disagreeCount = disagreeCount;
    }

    public Todo getTodo() {
        return todo;
    }

    public void setTodo(Todo todo) {
        this.todo = todo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Tips.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null) ? "<null>" : this.id));
        sb.append(',');
        sb.append("createdAt");
        sb.append('=');
        sb.append(((this.createdAt == null) ? "<null>" : this.createdAt));
        sb.append(',');
        sb.append("text");
        sb.append('=');
        sb.append(((this.text == null) ? "<null>" : this.text));
        sb.append(',');
        sb.append("type");
        sb.append('=');
        sb.append(((this.type == null) ? "<null>" : this.type));
        sb.append(',');
        sb.append("canonicalUrl");
        sb.append('=');
        sb.append(((this.canonicalUrl == null) ? "<null>" : this.canonicalUrl));
        sb.append(',');
        sb.append("photo");
        sb.append('=');
        sb.append(((this.photo == null) ? "<null>" : this.photo));
        sb.append(',');
        sb.append("photourl");
        sb.append('=');
        sb.append(((this.photoUrl == null) ? "<null>" : this.photoUrl));
        sb.append(',');
        sb.append("likes");
        sb.append('=');
        sb.append(((this.likes == null) ? "<null>" : this.likes));
        sb.append(',');
        sb.append("logView");
        sb.append('=');
        sb.append(((this.logView == null) ? "<null>" : this.logView));
        sb.append(',');
        sb.append("agreeCount");
        sb.append('=');
        sb.append(((this.agreeCount == null) ? "<null>" : this.agreeCount));
        sb.append(',');
        sb.append("disagreeCount");
        sb.append('=');
        sb.append(((this.disagreeCount == null) ? "<null>" : this.disagreeCount));
        sb.append(',');
        sb.append("todo");
        sb.append('=');
        sb.append(((this.todo == null) ? "<null>" : this.todo));
        sb.append(',');
        sb.append("user");
        sb.append('=');
        sb.append(((this.user == null) ? "<null>" : this.user));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.canonicalUrl == null) ? 0 : this.canonicalUrl.hashCode()));
        result = ((result * 31) + ((this.photo == null) ? 0 : this.photo.hashCode()));
        result = ((result * 31) + ((this.type == null) ? 0 : this.type.hashCode()));
        result = ((result * 31) + ((this.disagreeCount == null) ? 0 : this.disagreeCount.hashCode()));
        result = ((result * 31) + ((this.todo == null) ? 0 : this.todo.hashCode()));
        result = ((result * 31) + ((this.createdAt == null) ? 0 : this.createdAt.hashCode()));
        result = ((result * 31) + ((this.photoUrl == null) ? 0 : this.photoUrl.hashCode()));
        result = ((result * 31) + ((this.logView == null) ? 0 : this.logView.hashCode()));
        result = ((result * 31) + ((this.agreeCount == null) ? 0 : this.agreeCount.hashCode()));
        result = ((result * 31) + ((this.id == null) ? 0 : this.id.hashCode()));
        result = ((result * 31) + ((this.text == null) ? 0 : this.text.hashCode()));
        result = ((result * 31) + ((this.user == null) ? 0 : this.user.hashCode()));
        result = ((result * 31) + ((this.likes == null) ? 0 : this.likes.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Tips) == false) {
            return false;
        }
        Tips rhs = ((Tips) other);
        return ((((((((((((((this.canonicalUrl == rhs.canonicalUrl) || ((this.canonicalUrl != null) && this.canonicalUrl.equals(rhs.canonicalUrl))) && ((this.photo == rhs.photo) || ((this.photo != null) && this.photo.equals(rhs.photo)))) && ((this.type == rhs.type) || ((this.type != null) && this.type.equals(rhs.type)))) && ((this.disagreeCount == rhs.disagreeCount) || ((this.disagreeCount != null) && this.disagreeCount.equals(rhs.disagreeCount)))) && ((this.todo == rhs.todo) || ((this.todo != null) && this.todo.equals(rhs.todo)))) && ((this.createdAt == rhs.createdAt) || ((this.createdAt != null) && this.createdAt.equals(rhs.createdAt)))) && ((this.photoUrl == rhs.photoUrl) || ((this.photoUrl != null) && this.photoUrl.equals(rhs.photoUrl)))) && ((this.logView == rhs.logView) || ((this.logView != null) && this.logView.equals(rhs.logView)))) && ((this.agreeCount == rhs.agreeCount) || ((this.agreeCount != null) && this.agreeCount.equals(rhs.agreeCount)))) && ((this.id == rhs.id) || ((this.id != null) && this.id.equals(rhs.id)))) && ((this.text == rhs.text) || ((this.text != null) && this.text.equals(rhs.text)))) && ((this.user == rhs.user) || ((this.user != null) && this.user.equals(rhs.user)))) && ((this.likes == rhs.likes) || ((this.likes != null) && this.likes.equals(rhs.likes))));
    }

}
