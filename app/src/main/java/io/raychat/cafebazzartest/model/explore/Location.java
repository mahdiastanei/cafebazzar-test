
package io.raychat.cafebazzartest.model.explore;

import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.LOCATION)
public class Location implements Parcelable {
    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Location.ADDRESS)
    @SerializedName("address")
    public String address = "";

    @ColumnInfo(name = DbConstants.ColumnNames.Location.CROSS_STREET)
    @SerializedName("crossStreet")
    public String crossStreet;

    @ColumnInfo(name = DbConstants.ColumnNames.Location.LAT)
    @SerializedName("lat")
    public Double lat;

    @ColumnInfo(name = DbConstants.ColumnNames.Location.LNG)
    @SerializedName("lng")
    public Double lng;

    @ColumnInfo(name = DbConstants.ColumnNames.Location.LABELED_LAT_LNGS)
    @SerializedName("labeledLatLngs")
    public List<LabeledLatLng> labeledLatLngs = null;

    @ColumnInfo(name = DbConstants.ColumnNames.Location.DISTANCE)
    @SerializedName("distance")
    public Integer distance;

    @ColumnInfo(name = DbConstants.ColumnNames.Location.POSTAL_CODE)
    @SerializedName("postalCode")
    public String postalCode;

    @ColumnInfo(name = DbConstants.ColumnNames.Location.CC)
    @SerializedName("cc")
    public String cc;

    @ColumnInfo(name = DbConstants.ColumnNames.Location.CITY)
    @SerializedName("city")
    public String city = "";

    @ColumnInfo(name = DbConstants.ColumnNames.Location.STATE)
    @SerializedName("state")
    public String state;

    @ColumnInfo(name = DbConstants.ColumnNames.Location.COUNTRY)
    @SerializedName("country")
    public String country;

    @ColumnInfo(name = DbConstants.ColumnNames.Location.FORMATTED_ADDRESS)
    @SerializedName("formattedAddress")
    public List<String> formattedAddress = null;

    public final static Creator<Location> CREATOR = new Creator<Location>() {


        public Location createFromParcel(android.os.Parcel in) {
            return new Location(in);
        }

        public Location[] newArray(int size) {
            return (new Location[size]);
        }

    };

    protected Location(android.os.Parcel in) {
        this.address = ((String) in.readValue((String.class.getClassLoader())));
        this.crossStreet = ((String) in.readValue((String.class.getClassLoader())));
        this.lat = ((Double) in.readValue((Double.class.getClassLoader())));
        this.lng = ((Double) in.readValue((Double.class.getClassLoader())));
        in.readList(this.labeledLatLngs, (io.raychat.cafebazzartest.model.explore.LabeledLatLng.class.getClassLoader()));
        this.distance = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.postalCode = ((String) in.readValue((String.class.getClassLoader())));
        this.cc = ((String) in.readValue((String.class.getClassLoader())));
        this.city = ((String) in.readValue((String.class.getClassLoader())));
        this.state = ((String) in.readValue((String.class.getClassLoader())));
        this.country = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.formattedAddress, (java.lang.String.class.getClassLoader()));
    }

    public Location() {
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCrossStreet() {
        return crossStreet;
    }

    public void setCrossStreet(String crossStreet) {
        this.crossStreet = crossStreet;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public List<LabeledLatLng> getLabeledLatLngs() {
        return labeledLatLngs;
    }

    public void setLabeledLatLngs(List<LabeledLatLng> labeledLatLngs) {
        this.labeledLatLngs = labeledLatLngs;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getFormattedAddress() {
        return formattedAddress;
    }

    public void setFormattedAddress(List<String> formattedAddress) {
        this.formattedAddress = formattedAddress;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Location.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("address");
        sb.append('=');
        sb.append(((this.address == null) ? "<null>" : this.address));
        sb.append(',');
        sb.append("crossStreet");
        sb.append('=');
        sb.append(((this.crossStreet == null) ? "<null>" : this.crossStreet));
        sb.append(',');
        sb.append("lat");
        sb.append('=');
        sb.append(((this.lat == null) ? "<null>" : this.lat));
        sb.append(',');
        sb.append("lng");
        sb.append('=');
        sb.append(((this.lng == null) ? "<null>" : this.lng));
        sb.append(',');
        sb.append("labeledLatLngs");
        sb.append('=');
        sb.append(((this.labeledLatLngs == null) ? "<null>" : this.labeledLatLngs));
        sb.append(',');
        sb.append("distance");
        sb.append('=');
        sb.append(((this.distance == null) ? "<null>" : this.distance));
        sb.append(',');
        sb.append("postalCode");
        sb.append('=');
        sb.append(((this.postalCode == null) ? "<null>" : this.postalCode));
        sb.append(',');
        sb.append("cc");
        sb.append('=');
        sb.append(((this.cc == null) ? "<null>" : this.cc));
        sb.append(',');
        sb.append("city");
        sb.append('=');
        sb.append(((this.city == null) ? "<null>" : this.city));
        sb.append(',');
        sb.append("state");
        sb.append('=');
        sb.append(((this.state == null) ? "<null>" : this.state));
        sb.append(',');
        sb.append("country");
        sb.append('=');
        sb.append(((this.country == null) ? "<null>" : this.country));
        sb.append(',');
        sb.append("formattedAddress");
        sb.append('=');
        sb.append(((this.formattedAddress == null) ? "<null>" : this.formattedAddress));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.cc == null) ? 0 : this.cc.hashCode()));
        result = ((result * 31) + ((this.country == null) ? 0 : this.country.hashCode()));
        result = ((result * 31) + ((this.address == null) ? 0 : this.address.hashCode()));
        result = ((result * 31) + ((this.lng == null) ? 0 : this.lng.hashCode()));
        result = ((result * 31) + ((this.distance == null) ? 0 : this.distance.hashCode()));
        result = ((result * 31) + ((this.city == null) ? 0 : this.city.hashCode()));
        result = ((result * 31) + ((this.postalCode == null) ? 0 : this.postalCode.hashCode()));
        result = ((result * 31) + ((this.labeledLatLngs == null) ? 0 : this.labeledLatLngs.hashCode()));
        result = ((result * 31) + ((this.formattedAddress == null) ? 0 : this.formattedAddress.hashCode()));
        result = ((result * 31) + ((this.state == null) ? 0 : this.state.hashCode()));
        result = ((result * 31) + ((this.crossStreet == null) ? 0 : this.crossStreet.hashCode()));
        result = ((result * 31) + ((this.lat == null) ? 0 : this.lat.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Location) == false) {
            return false;
        }
        Location rhs = ((Location) other);
        return (((((((((((((this.cc == rhs.cc) || ((this.cc != null) && this.cc.equals(rhs.cc))) && ((this.country == rhs.country) || ((this.country != null) && this.country.equals(rhs.country)))) && ((this.address == rhs.address) || ((this.address != null) && this.address.equals(rhs.address)))) && ((this.lng == rhs.lng) || ((this.lng != null) && this.lng.equals(rhs.lng)))) && ((this.distance == rhs.distance) || ((this.distance != null) && this.distance.equals(rhs.distance)))) && ((this.city == rhs.city) || ((this.city != null) && this.city.equals(rhs.city)))) && ((this.postalCode == rhs.postalCode) || ((this.postalCode != null) && this.postalCode.equals(rhs.postalCode)))) && ((this.labeledLatLngs == rhs.labeledLatLngs) || ((this.labeledLatLngs != null) && this.labeledLatLngs.equals(rhs.labeledLatLngs)))) && ((this.formattedAddress == rhs.formattedAddress) || ((this.formattedAddress != null) && this.formattedAddress.equals(rhs.formattedAddress)))) && ((this.state == rhs.state) || ((this.state != null) && this.state.equals(rhs.state)))) && ((this.crossStreet == rhs.crossStreet) || ((this.crossStreet != null) && this.crossStreet.equals(rhs.crossStreet)))) && ((this.lat == rhs.lat) || ((this.lat != null) && this.lat.equals(rhs.lat))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(address);
        dest.writeValue(crossStreet);
        dest.writeValue(lat);
        dest.writeValue(lng);
        dest.writeList(labeledLatLngs);
        dest.writeValue(distance);
        dest.writeValue(postalCode);
        dest.writeValue(cc);
        dest.writeValue(city);
        dest.writeValue(state);
        dest.writeValue(country);
        dest.writeList(formattedAddress);
    }

    public int describeContents() {
        return 0;
    }

}
