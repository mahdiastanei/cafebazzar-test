
package io.raychat.cafebazzartest.model.explore;


import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.CATEGORY)
public class Category implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Category.ID)
    @SerializedName("id")
    public String id;

    @ColumnInfo(name = DbConstants.ColumnNames.Category.NAME)
    @SerializedName("name")
    public String name ="";

    @ColumnInfo(name = DbConstants.ColumnNames.Category.PLURAL_NAME)
    @SerializedName("pluralName")
    public String pluralName;

    @ColumnInfo(name = DbConstants.ColumnNames.Category.SHORT_NAME)
    @SerializedName("shortName")
    public String shortName;

    @Embedded(prefix = DbConstants.Prefixes.ICON)
    @SerializedName("icon")
    public Icon icon;

    @ColumnInfo(name = DbConstants.ColumnNames.Category.PRIMARY)
    @SerializedName("primary")
    public Boolean primary;

    public final static Creator<Category> CREATOR = new Creator<Category>() {


        public Category createFromParcel(android.os.Parcel in) {
            return new Category(in);
        }

        public Category[] newArray(int size) {
            return (new Category[size]);
        }

    };

    protected Category(android.os.Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.pluralName = ((String) in.readValue((String.class.getClassLoader())));
        this.shortName = ((String) in.readValue((String.class.getClassLoader())));
        this.icon = ((Icon) in.readValue((Icon.class.getClassLoader())));
        this.primary = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
    }

    public Category() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPluralName() {
        return pluralName;
    }

    public void setPluralName(String pluralName) {
        this.pluralName = pluralName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public Boolean getPrimary() {
        return primary;
    }

    public void setPrimary(Boolean primary) {
        this.primary = primary;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Category.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null) ? "<null>" : this.id));
        sb.append(',');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null) ? "<null>" : this.name));
        sb.append(',');
        sb.append("pluralName");
        sb.append('=');
        sb.append(((this.pluralName == null) ? "<null>" : this.pluralName));
        sb.append(',');
        sb.append("shortName");
        sb.append('=');
        sb.append(((this.shortName == null) ? "<null>" : this.shortName));
        sb.append(',');
        sb.append("icon");
        sb.append('=');
        sb.append(((this.icon == null) ? "<null>" : this.icon));
        sb.append(',');
        sb.append("primary");
        sb.append('=');
        sb.append(((this.primary == null) ? "<null>" : this.primary));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.pluralName == null) ? 0 : this.pluralName.hashCode()));
        result = ((result * 31) + ((this.name == null) ? 0 : this.name.hashCode()));
        result = ((result * 31) + ((this.icon == null) ? 0 : this.icon.hashCode()));
        result = ((result * 31) + ((this.id == null) ? 0 : this.id.hashCode()));
        result = ((result * 31) + ((this.shortName == null) ? 0 : this.shortName.hashCode()));
        result = ((result * 31) + ((this.primary == null) ? 0 : this.primary.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Category) == false) {
            return false;
        }
        Category rhs = ((Category) other);
        return (((((((this.pluralName == rhs.pluralName) || ((this.pluralName != null) && this.pluralName.equals(rhs.pluralName))) && ((this.name == rhs.name) || ((this.name != null) && this.name.equals(rhs.name)))) && ((this.icon == rhs.icon) || ((this.icon != null) && this.icon.equals(rhs.icon)))) && ((this.id == rhs.id) || ((this.id != null) && this.id.equals(rhs.id)))) && ((this.shortName == rhs.shortName) || ((this.shortName != null) && this.shortName.equals(rhs.shortName)))) && ((this.primary == rhs.primary) || ((this.primary != null) && this.primary.equals(rhs.primary))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(pluralName);
        dest.writeValue(shortName);
        dest.writeValue(icon);
        dest.writeValue(primary);
    }

    public int describeContents() {
        return 0;
    }

}
