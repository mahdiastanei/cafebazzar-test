
package io.raychat.cafebazzartest.model.explore.main;


import android.os.Parcelable;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import io.raychat.cafebazzartest.model.explore.Meta;
import io.raychat.cafebazzartest.model.explore.Response;
import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.EXPLORE)
public class ExploreData implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    public int tableId;

    @Embedded(prefix = DbConstants.Prefixes.META)
    @SerializedName("meta")
    public Meta meta;

    @Embedded(prefix = DbConstants.Prefixes.RESPONSE)
    @SerializedName("response")
    public Response response;

    public final static Creator<ExploreData> CREATOR = new Creator<ExploreData>() {


        public ExploreData createFromParcel(android.os.Parcel in) {
            return new ExploreData(in);
        }

        public ExploreData[] newArray(int size) {
            return (new ExploreData[size]);
        }

    };

    protected ExploreData(android.os.Parcel in) {
        this.meta = ((Meta) in.readValue((Meta.class.getClassLoader())));
        this.response = ((Response) in.readValue((Response.class.getClassLoader())));
    }

    public ExploreData() {
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public int getId() {
        return tableId;
    }

    public void setId(int id) {
        this.tableId = tableId;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ExploreData.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("meta");
        sb.append('=');
        sb.append(((this.meta == null) ? "<null>" : this.meta));
        sb.append(',');
        sb.append("response");
        sb.append('=');
        sb.append(((this.response == null) ? "<null>" : this.response));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.meta == null) ? 0 : this.meta.hashCode()));
        result = ((result * 31) + ((this.response == null) ? 0 : this.response.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if (!(other instanceof ExploreData)) {
            return false;
        }
        ExploreData rhs = ((ExploreData) other);
        return ((Objects.equals(this.meta, rhs.meta)) && (Objects.equals(this.response, rhs.response)));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(meta);
        dest.writeValue(response);
    }

    public int describeContents() {
        return 0;
    }

}
