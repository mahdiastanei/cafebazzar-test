
package io.raychat.cafebazzartest.model.explore;

import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.GROUP)
public class Group implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Group.TYPE)
    @SerializedName("type")
    public String type;

    @ColumnInfo(name = DbConstants.ColumnNames.Group.NAME)
    @SerializedName("name")
    public String name;

    @ColumnInfo(name = DbConstants.ColumnNames.Group.ITEMS)
    @SerializedName("items")
    public List<GroupItem> groupItems = null;

    public final static Creator<Group> CREATOR = new Creator<Group>() {


        public Group createFromParcel(android.os.Parcel in) {
            return new Group(in);
        }

        public Group[] newArray(int size) {
            return (new Group[size]);
        }

    };

    protected Group(android.os.Parcel in) {
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.groupItems, (GroupItem.class.getClassLoader()));
    }

    public Group() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GroupItem> getGroupItems() {
        return groupItems;
    }

    public void setGroupItems(List<GroupItem> groupItems) {
        this.groupItems = groupItems;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Group.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("type");
        sb.append('=');
        sb.append(((this.type == null) ? "<null>" : this.type));
        sb.append(',');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null) ? "<null>" : this.name));
        sb.append(',');
        sb.append("items");
        sb.append('=');
        sb.append(((this.groupItems == null) ? "<null>" : this.groupItems));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.name == null) ? 0 : this.name.hashCode()));
        result = ((result * 31) + ((this.type == null) ? 0 : this.type.hashCode()));
        result = ((result * 31) + ((this.groupItems == null) ? 0 : this.groupItems.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Group) == false) {
            return false;
        }
        Group rhs = ((Group) other);
        return ((((this.name == rhs.name) || ((this.name != null) && this.name.equals(rhs.name))) && ((this.type == rhs.type) || ((this.type != null) && this.type.equals(rhs.type)))) && ((this.groupItems == rhs.groupItems) || ((this.groupItems != null) && this.groupItems.equals(rhs.groupItems))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(type);
        dest.writeValue(name);
        dest.writeList(groupItems);
    }

    public int describeContents() {
        return 0;
    }

}
