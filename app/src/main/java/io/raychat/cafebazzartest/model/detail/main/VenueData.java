
package io.raychat.cafebazzartest.model.detail.main;


import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.model.detail.Meta;
import io.raychat.cafebazzartest.model.detail.Response;


public class VenueData {

    @SerializedName("meta")
    private Meta meta;
    @SerializedName("response")
    private Response response;

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(VenueData.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("meta");
        sb.append('=');
        sb.append(((this.meta == null)?"<null>":this.meta));
        sb.append(',');
        sb.append("response");
        sb.append('=');
        sb.append(((this.response == null)?"<null>":this.response));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.meta == null)? 0 :this.meta.hashCode()));
        result = ((result* 31)+((this.response == null)? 0 :this.response.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof VenueData) == false) {
            return false;
        }
        VenueData rhs = ((VenueData) other);
        return (((this.meta == rhs.meta)||((this.meta!= null)&&this.meta.equals(rhs.meta)))&&((this.response == rhs.response)||((this.response!= null)&&this.response.equals(rhs.response))));
    }

}
