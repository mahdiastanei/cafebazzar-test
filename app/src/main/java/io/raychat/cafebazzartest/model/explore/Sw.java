
package io.raychat.cafebazzartest.model.explore;


import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import io.raychat.cafebazzartest.utils.constants.DbConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Entity(tableName = DbConstants.TableNames.SW)
public class Sw implements Parcelable {

    @PrimaryKey(autoGenerate = false)
    public int tableId;

    @ColumnInfo(name = DbConstants.ColumnNames.Sw.LAT)
    @SerializedName("lat")
    public Double lat;

    @ColumnInfo(name = DbConstants.ColumnNames.Sw.LNG)
    @SerializedName("lng")
    public Double lng;

    public final static Creator<Sw> CREATOR = new Creator<Sw>() {


        public Sw createFromParcel(android.os.Parcel in) {
            return new Sw(in);
        }

        public Sw[] newArray(int size) {
            return (new Sw[size]);
        }

    };

    protected Sw(android.os.Parcel in) {
        this.lat = ((Double) in.readValue((Double.class.getClassLoader())));
        this.lng = ((Double) in.readValue((Double.class.getClassLoader())));
    }

    public Sw() {
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Sw.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("lat");
        sb.append('=');
        sb.append(((this.lat == null) ? "<null>" : this.lat));
        sb.append(',');
        sb.append("lng");
        sb.append('=');
        sb.append(((this.lng == null) ? "<null>" : this.lng));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.lng == null) ? 0 : this.lng.hashCode()));
        result = ((result * 31) + ((this.lat == null) ? 0 : this.lat.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Sw) == false) {
            return false;
        }
        Sw rhs = ((Sw) other);
        return (((this.lng == rhs.lng) || ((this.lng != null) && this.lng.equals(rhs.lng))) && ((this.lat == rhs.lat) || ((this.lat != null) && this.lat.equals(rhs.lat))));
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(lat);
        dest.writeValue(lng);
    }

    public int describeContents() {
        return 0;
    }

}
