package io.raychat.cafebazzartest.utils.room.dataSource.myLocation;

import androidx.lifecycle.LiveData;

import io.raychat.cafebazzartest.model.MyLocation;
import io.raychat.cafebazzartest.utils.room.dao.MyLocationDAO;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class MyLocationDataSource implements MyLocationLocalSource {

    private final MyLocationDAO myLocationDAO;

    public MyLocationDataSource(MyLocationDAO myLocationDAO) {
        this.myLocationDAO = myLocationDAO;
    }


    @Override
    public Single<MyLocation> getUserLastLocation() {
        return myLocationDAO.getMyLocationSingle();
    }

    @Override
    public Flowable<MyLocation> getMyLocationFlowable() {
        return myLocationDAO.getMyLocationFlowable();
    }

    @Override
    public LiveData<MyLocation> getMyLocationLiveData() {
        return myLocationDAO.getMyLocationLiveData();
    }

    @Override
    public MyLocation getMyLocation() {
        return myLocationDAO.getMyLocation();
    }

    @Override
    public void insertMyLocation(MyLocation myLocation) {
        new Thread(() -> myLocationDAO.insertMyLocation(myLocation)).start();
    }

    @Override
    public void clear() {
        new Thread(myLocationDAO::clearTable).start();
    }
}
