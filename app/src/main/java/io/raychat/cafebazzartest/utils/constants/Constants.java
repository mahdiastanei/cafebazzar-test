package io.raychat.cafebazzartest.utils.constants;

/**
 * Created by Alireza Nezami on 4/6/2021.
 */
public class Constants {

    public static final String UPDATE_INTERVAL_VALUE = "update_interval_value";
    public static final String DISTANCE_TO_REFRESH_VALUE = "distance_to_refresh_value";
    public static final String CLIENT_ID = "MJ0IYDGHRGQPEYL3BCDP4HUMSQJMZ2HGQLJNHNJYVGIG0D3D";
    public static final String CLIENT_SECRET = "4GMRTUCHQMF0PD1NTHZYTQRDA3DGEY2EIHWIS43ZF3NTBRX5";
    public static final String V = "20210324";

    public interface IntentConstants {
        String VENUE_ID = "venueId";
        String VENUE_DATA = "venueData";
    }
}
