package io.raychat.cafebazzartest.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import java.text.DateFormat;
import java.util.Date;

import io.raychat.cafebazzartest.R;
import io.raychat.cafebazzartest.model.MyLocation;

public class Utils {
    private static String TAG = Utils.class.getName();
    public static final String KEY_NEAR_ME_ENABLED = "requesting_locaction_updates";


    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    public static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_NEAR_ME_ENABLED, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     *
     * @param requestingLocationUpdates The location updates state.
     */
    public static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_NEAR_ME_ENABLED, requestingLocationUpdates)
                .apply();
    }

    /**
     * Returns the {@code location} object as a human readable string.
     *
     * @param location The {@link Location}.
     */
    public static String getLocationCoordinates(MyLocation location) {
        Log.i(TAG, "getLocationCoordinates: " + "(" + location.getLatitude() +
                ", " + location.getLongitude() + ")");
        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ",     " + location.getDistance() +
                        ")";

    }

    public static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated,
                DateFormat.getDateTimeInstance().format(new Date()));
    }

    /**
     * Returns the current state of the permissions needed.
     */
    public static boolean checkPermissions(Context context) {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }
}