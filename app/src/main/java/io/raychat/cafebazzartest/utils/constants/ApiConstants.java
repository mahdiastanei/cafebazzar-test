package io.raychat.cafebazzartest.utils.constants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class ApiConstants {

    public static final String BASE_URL = "https://api.foursquare.com/v2/";

    public interface Parameters {
        String CLIENT_ID = "client_id";
        String CLIENT_SECRET = "client_secret";
        String V = "v";
        String RADIUS = "radius";
        String LIMIT = "limit";
        String OFFSET = "offset";
        String LOCATION = "ll";
        String SORT_BY_DISTANCE = "sortByDistance";
    }

    public interface ParameterDefaultValues {
        int radius = 1000; // in meter
        int sortByDistance = 1; // 0 for false
        int limit = 30; // number of venues receiving

    }

    public interface ApiUrls {
        String EXPLORE = "venues/explore/";
        String VENUES = "venues/";
        String PHOTOS = "/photos";
    }


    public enum ApiFlags {
        NEW_EXPLORE(1000),
        OFFSET_EXPLORE(1001),
        VENUE_DETAIL(1002),
        PHOTOS(1003);

        public int value;

        ApiFlags(int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }

        public void clearValue() {
            value = 0;
        }
    }

    public interface ResponseCodes {
        int CONFLICT = 409;
        int NOT_FOUND = 404;
        int UN_AUTHORIZED = 400;
        int SUCCESSFUL = 200;
    }

}
