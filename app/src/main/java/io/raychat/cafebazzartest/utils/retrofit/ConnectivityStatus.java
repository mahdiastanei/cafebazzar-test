package io.raychat.cafebazzartest.utils.retrofit;

import android.content.Context;
import android.content.ContextWrapper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class ConnectivityStatus extends ContextWrapper {

    public ConnectivityStatus(Context base) {
        super(base);
    }

    public static boolean isConnected(Context context) {

        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo connection = null;
        if (manager != null) {
            connection = manager.getActiveNetworkInfo();
        }
        return connection != null && connection.isConnectedOrConnecting();
    }
}