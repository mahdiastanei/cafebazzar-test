package io.raychat.cafebazzartest.utils.room;

import android.content.Context;

import io.raychat.cafebazzartest.utils.room.dataSource.explore.ExploreDataSource;
import io.raychat.cafebazzartest.utils.room.dataSource.myLocation.MyLocationDataSource;
import io.raychat.cafebazzartest.utils.room.injection.Injection;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class LocalDatabase {


    public static ExploreDataSource exploreDataSource;
    public static MyLocationDataSource myLocationDataSource;

    private Context context;

    public LocalDatabase(Context context) {
        this.context = context;
    }

    public void initAppDatabase() {
        Injection.createDatabase(context);
        exploreDataSource = Injection.provideExploreDataSource(context);
        myLocationDataSource = Injection.provideMyLocationDataSource(context);
    }

}
