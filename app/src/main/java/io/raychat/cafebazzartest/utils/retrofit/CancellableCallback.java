package io.raychat.cafebazzartest.utils.retrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class CancellableCallback<ResponseBody> implements Callback<okhttp3.ResponseBody> {

    private Callback<okhttp3.ResponseBody> callback;

    private boolean canceled;

    private CancellableCallback() {
    }

    public CancellableCallback(Callback<okhttp3.ResponseBody> callback) {
        this.callback = callback;
        canceled = false;
    }

    public void cancel() {
        canceled = true;
        callback = null;
    }

    @Override
    public void onResponse(Call<okhttp3.ResponseBody> call, Response<okhttp3.ResponseBody> response) {
        if (!canceled) {
            callback.onResponse(call, response);
        }
    }

    @Override
    public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {
        if (!canceled) {
            callback.onFailure(call, t);
        }
    }
}
