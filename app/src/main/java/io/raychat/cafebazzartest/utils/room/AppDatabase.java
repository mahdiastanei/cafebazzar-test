package io.raychat.cafebazzartest.utils.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import io.raychat.cafebazzartest.model.MyLocation;
import io.raychat.cafebazzartest.model.detail.Stats;
import io.raychat.cafebazzartest.model.explore.Category;
import io.raychat.cafebazzartest.model.explore.Group;
import io.raychat.cafebazzartest.model.explore.GroupItem;
import io.raychat.cafebazzartest.model.explore.Icon;
import io.raychat.cafebazzartest.model.explore.Item;
import io.raychat.cafebazzartest.model.explore.LabeledLatLng;
import io.raychat.cafebazzartest.model.explore.Location;
import io.raychat.cafebazzartest.model.explore.Meta;
import io.raychat.cafebazzartest.model.explore.Ne;
import io.raychat.cafebazzartest.model.explore.PhotoGroup;
import io.raychat.cafebazzartest.model.explore.Photos;
import io.raychat.cafebazzartest.model.explore.Reasons;
import io.raychat.cafebazzartest.model.explore.Response;
import io.raychat.cafebazzartest.model.explore.SuggestedBounds;
import io.raychat.cafebazzartest.model.explore.Sw;
import io.raychat.cafebazzartest.model.explore.Tips;
import io.raychat.cafebazzartest.model.explore.User;
import io.raychat.cafebazzartest.model.explore.Venue;
import io.raychat.cafebazzartest.model.explore.VenuePage;
import io.raychat.cafebazzartest.model.explore.Warning;
import io.raychat.cafebazzartest.model.explore.main.ExploreData;
import io.raychat.cafebazzartest.utils.constants.DbConstants;
import io.raychat.cafebazzartest.utils.room.converters.CategoryConverter;
import io.raychat.cafebazzartest.utils.room.converters.FormattedAddressConverter;
import io.raychat.cafebazzartest.utils.room.converters.GroupItemConverter;
import io.raychat.cafebazzartest.utils.room.converters.ItemConverter;
import io.raychat.cafebazzartest.utils.room.converters.LabeledLatLngsConverter;
import io.raychat.cafebazzartest.utils.room.converters.PhotoGroupConverter;
import io.raychat.cafebazzartest.utils.room.converters.TipsConverter;
import io.raychat.cafebazzartest.utils.room.dao.ExploreDAO;
import io.raychat.cafebazzartest.utils.room.dao.MyLocationDAO;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Database(entities = {
        ExploreData.class,
        Category.class,
        Group.class,
        PhotoGroup.class,
        User.class,
        Tips.class,
        Stats.class,
        Photos.class,
        GroupItem.class,
        Icon.class,
        Item.class,
        LabeledLatLng.class,
        Location.class,
        Meta.class,
        Ne.class,
        Reasons.class,
        Response.class,
        SuggestedBounds.class,
        Sw.class,
        Venue.class,
        VenuePage.class,
        Warning.class,
        MyLocation.class
}, version = 1, exportSchema = false)

@TypeConverters({CategoryConverter.class,
        FormattedAddressConverter.class,
        TipsConverter.class,
        GroupItemConverter.class,
        ItemConverter.class,
        PhotoGroupConverter.class,
        LabeledLatLngsConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;


    public abstract ExploreDAO exploreDao();

    public abstract MyLocationDAO myLocationDAO();


    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class,
                            DbConstants.MAIN_DB_NAME)
                            .build();
                }


            }
        }

        return INSTANCE;

    }


    public static void destroyInstance() {
        INSTANCE = null;
    }

}
