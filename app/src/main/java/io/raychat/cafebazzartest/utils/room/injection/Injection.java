/*
 * Copyright 2018 Mayur Rokade
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

package io.raychat.cafebazzartest.utils.room.injection;

import android.content.Context;

import io.raychat.cafebazzartest.utils.room.AppDatabase;
import io.raychat.cafebazzartest.utils.room.dataSource.explore.ExploreDataSource;
import io.raychat.cafebazzartest.utils.room.dataSource.myLocation.MyLocationDataSource;
import io.raychat.cafebazzartest.utils.room.schedulers.BaseSchedulerProvider;
import io.raychat.cafebazzartest.utils.room.schedulers.SchedulerProvider;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class Injection {

    public static AppDatabase mainDatabase = null;

    public static BaseSchedulerProvider provideSchedulerProvider() {
        return SchedulerProvider.getInstance();
    }

    public static void createDatabase(Context context) {
        mainDatabase = AppDatabase.getAppDatabase(context);
    }

    /*
     * Injecting Explore Data source
     */
    public static ExploreDataSource provideExploreDataSource(Context context) {
        if (mainDatabase == null)
            mainDatabase = AppDatabase.getAppDatabase(context);
        return new ExploreDataSource(mainDatabase.exploreDao());
    }

    /*
     * Injecting MyLocation Data source
     */
    public static MyLocationDataSource provideMyLocationDataSource(Context context) {
        if (mainDatabase == null)
            mainDatabase = AppDatabase.getAppDatabase(context);
        return new MyLocationDataSource(mainDatabase.myLocationDAO());
    }
}
