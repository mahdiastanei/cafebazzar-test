package io.raychat.cafebazzartest.utils.retrofit;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import io.raychat.cafebazzartest.BuildConfig;
import io.raychat.cafebazzartest.utils.constants.ApiConstants;
import io.raychat.cafebazzartest.utils.constants.RetrofitConstants;
import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class RetrofitBase {
    private static final String TAG = RetrofitBase.class.getName();
    protected Retrofit retrofit;
    protected Context context;

    public RetrofitBase(Context context) {
        init(context, false);
    }


    private void init(Context context, boolean isUploading) {
        Log.i(TAG, "RetrofitBase: " + System.currentTimeMillis());
        this.context = context;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            interceptor.level(HttpLoggingInterceptor.Level.BODY);
        } else {
            interceptor.level(HttpLoggingInterceptor.Level.NONE);
        }

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient().newBuilder().addInterceptor(interceptor);
        httpClientBuilder.addInterceptor(new NetworkInterceptor(context));
        ExecutorService exec = new ThreadPoolExecutor(20, 20, 1, TimeUnit.HOURS, new LinkedBlockingQueue<>());
        Dispatcher d = new Dispatcher(exec);
        d.setMaxRequestsPerHost(20);
        httpClientBuilder.dispatcher(d);

        if (!isUploading) {
            httpClientBuilder.readTimeout(RetrofitConstants.TimeOut.SOCKET_TIME_OUT, TimeUnit.SECONDS);
            httpClientBuilder.connectTimeout(RetrofitConstants.TimeOut.CONNECTION_TIME_OUT, TimeUnit.SECONDS);
        } else {
            httpClientBuilder
                    .readTimeout(RetrofitConstants.TimeOut.UPLOAD_SOCKET_TIMEOUT, TimeUnit.SECONDS)
                    .connectTimeout(RetrofitConstants.TimeOut.UPLOAD_CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(RetrofitConstants.TimeOut.UPLOAD_CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                    .callTimeout(RetrofitConstants.TimeOut.UPLOAD_CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        }
        addHeaders(httpClientBuilder);

        OkHttpClient httpClient = httpClientBuilder.build();
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .client(httpClient)
                .build();
    }

    private void addHeaders(OkHttpClient.Builder builder) {
        final String connection = "keep-alive";
        final String accept = "*/*";

        builder.interceptors().add(chain -> {
            Request request = chain.request().newBuilder()
                    .addHeader("Connection", connection)
                    .addHeader("Accept", accept)
                    .build();
            return chain.proceed(request);
        });
    }

    void validateResponse(Response response, ApiResponseListener apiResponseListener, ApiConstants.ApiFlags apiFlag, long eventId) {
        if (response.code() == 200) {
            ResponseBody responseBody = (ResponseBody) response.body();
            try {
                apiResponseListener.onResponseSuccess(responseBody != null ? responseBody.string() : null, apiFlag);
            } catch (IOException e) {
                error(response, apiResponseListener, apiFlag);
            }
        } else {
            error(response, apiResponseListener, apiFlag);
        }
    }

    private void error(Response response, ApiResponseListener apiResponseListener, ApiConstants.ApiFlags apiFlag) {
        Gson gson = new Gson();
//        ApiStatus apiStatusPojo;
//        try {
//            apiStatusPojo = gson.fromJson((response.errorBody()).string(), ApiStatus.class);
//            apiStatusPojo.setResponseCode(response.code());
//            if (apiStatusPojo == null) {
//                apiStatusPojo = HttpUtil.getServerErrorPojo(context);
//            }
//            apiResponseListener.onResponseError(apiStatusPojo, null, apiFlag);
//        } catch (Exception e) {
//            apiResponseListener.onResponseError(HttpUtil.getServerErrorPojo(context), null, apiFlag);
//        }
    }
}