package io.raychat.cafebazzartest.utils.location;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import io.raychat.cafebazzartest.model.MyLocation;
import io.raychat.cafebazzartest.services.LocationUpdatesService;
import io.raychat.cafebazzartest.ui.explore.ExploreEventListener;
import io.raychat.cafebazzartest.utils.Utils;
import io.raychat.cafebazzartest.utils.room.LocalDatabase;

/**
 * Created by Alireza Nezami on 4/6/2021.
 * Receiver for broadcasts sent by {@link LocationUpdatesService}.
 */
public class LocationBroadcastReceiver extends BroadcastReceiver {

    private ExploreEventListener eventListener;

    public void setEventListener(ExploreEventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        MyLocation location = intent.getParcelableExtra(LocationUpdatesService.EXTRA_LOCATION);
        if (location != null && eventListener != null) {
            LocalDatabase.myLocationDataSource.insertMyLocation(location);
            Log.i("TAG", "onReceive: " + location.latitude + "," + location.longitude);
//            eventListener.onUserLocationChanged(location);
            Toast.makeText(context, Utils.getLocationCoordinates(location),
                    Toast.LENGTH_SHORT).show();
        }
    }


}
