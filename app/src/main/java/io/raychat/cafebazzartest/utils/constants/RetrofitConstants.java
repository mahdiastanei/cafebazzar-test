package io.raychat.cafebazzartest.utils.constants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class RetrofitConstants {
    public interface TimeOut {
        int UPLOAD_CONNECTION_TIMEOUT = 20000;
        int UPLOAD_SOCKET_TIMEOUT = 20000;
        int SOCKET_TIME_OUT = 60;
        int CONNECTION_TIME_OUT = 60;
    }

    public interface ErrorClass {
        String CODE = "code";
        String STATUS = "status";
        String MESSAGE = "message";
        String DEVELOPER_MESSAGE = "developerMessage";
    }

}
