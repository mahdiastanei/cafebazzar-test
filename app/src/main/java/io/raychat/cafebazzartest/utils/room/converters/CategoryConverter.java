package io.raychat.cafebazzartest.utils.room.converters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import io.raychat.cafebazzartest.model.explore.Category;
import io.raychat.cafebazzartest.model.explore.Group;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class CategoryConverter {
    @TypeConverter
    public static List<Category> fromString(String value) {
        Type listType = new TypeToken<List<Category>>() {
        }.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromArrayList(List<Category> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }
}
