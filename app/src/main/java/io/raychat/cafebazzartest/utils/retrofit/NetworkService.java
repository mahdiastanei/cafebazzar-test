package io.raychat.cafebazzartest.utils.retrofit;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.raychat.cafebazzartest.utils.constants.ApiConstants;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class NetworkService extends RetrofitBase {
    private String TAG = NetworkService.class.getName();

    private String url = "";
    private String query = "";
    private StringBuilder finalUrl = new StringBuilder();

    private ApiConstants.ApiFlags apiFlag;
    private int callMethod;

    private Map<String, Object> parameter = new HashMap<>();
    private List<String> fields = new ArrayList<>();
    private List<RequestBody> requestBodiesList;

    private RetrofitAPIs apiServices;
    private static Call<ResponseBody> currentApiCall;
    private ApiResponseListener apiResponseListener;
    private RequestBody body;
    private MultipartBody.Part file;


    public NetworkService setUrl(String url) {
        this.url = url;
        return this;
    }

    public NetworkService setQuery(String query) {
        this.query = query;
        return this;
    }

    public NetworkService setFinalUrl(StringBuilder finalUrl) {
        this.finalUrl = finalUrl;
        return this;
    }

    public NetworkService setApiFlag(ApiConstants.ApiFlags apiFlag) {
        this.apiFlag = apiFlag;
        return this;
    }

    public NetworkService setCallMethod(int callMethod) {
        this.callMethod = callMethod;
        return this;
    }

    public NetworkService setParameter(Map<String, Object> parameter) {
        this.parameter = parameter;
        return this;
    }

    public NetworkService setFields(List<String> fields) {
        this.fields = fields;
        return this;
    }

    public NetworkService setRequestBodiesList(List<RequestBody> requestBodiesList) {
        this.requestBodiesList = requestBodiesList;
        return this;
    }

    public NetworkService setApiServices(RetrofitAPIs apiServices) {
        this.apiServices = apiServices;
        return this;
    }


    public NetworkService setApiResponseListener(ApiResponseListener apiResponseListener) {
        this.apiResponseListener = apiResponseListener;
        return this;
    }

    public NetworkService setBody(RequestBody body) {
        this.body = body;
        return this;
    }

    public NetworkService setFile(MultipartBody.Part file) {
        this.file = file;
        return this;
    }

    public NetworkService addParameter(String key, Object value) {
        this.parameter.put(key, value);
        return this;
    }

    public NetworkService addFields(String... fieldsName) {
        fields.addAll(Arrays.asList(fieldsName));
        return this;
    }

    public NetworkService addBodies(RequestBody... requestBodies) {
        requestBodiesList.addAll(Arrays.asList(requestBodies));
        return this;
    }

    public NetworkService(Context context) {
        super(context);
        apiServices = retrofit.create(RetrofitAPIs.class);

    }

    public void get() {
        finalUrl.append(url);
        if (parameter != null && !parameter.isEmpty()) {
            if (!TextUtils.isEmpty(query))
                finalUrl.append(query).append("?");
            else
                finalUrl.append("?");

            Set keys = parameter.keySet();

            for (Iterator i = keys.iterator(); i.hasNext(); ) {
                String key = (String) i.next();
                finalUrl.append(key).append("=").append(parameter.get(key));
                if (i.hasNext())
                    finalUrl.append("&");
            }

            if (fields.size() > 0) {
                String field = TextUtils.join(",", fields);
                finalUrl.append("&").append("fields=").append(field);
            }

            get(String.valueOf(finalUrl), apiResponseListener, apiFlag, System.nanoTime());

        } else if (!TextUtils.isEmpty(query)) {
            finalUrl.append(url).append(query);
            if (fields != null && fields.size() > 0) {
                String field = TextUtils.join(",", fields);
                finalUrl.append("&").append("fields=").append(field);
            }
            get(String.valueOf(finalUrl), apiResponseListener, apiFlag, System.nanoTime());
        } else {
            get(String.valueOf(finalUrl), apiResponseListener, apiFlag, System.nanoTime());
        }

        clear();
    }

    private void clear() {
        parameter = new HashMap<>();
        requestBodiesList = new ArrayList<>();
        fields = new ArrayList<>();
        apiFlag.clearValue();
        finalUrl = new StringBuilder();
        query = "";
        url = "";
        file = null;
    }


    @EverythingIsNonNull
    private void get(String url, final ApiResponseListener apiResponseListener, ApiConstants.ApiFlags apiFlag,
                     long eventId) {

        Call<ResponseBody> call = apiServices.get(url);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, "onResponse: success" + NetworkService.this.apiFlag);
                validateResponse(response, apiResponseListener, apiFlag, eventId);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.i(TAG, "onResponse fail: " + t.toString() + " flag: " + NetworkService.this.apiFlag);
//                apiResponseListener.onResponseError(HttpUtil.getServerErrorPojo(context), t, apiFlag);
            }
        });
    }


    public static boolean cancelCurrentCall() {
        currentApiCall.cancel();
        return currentApiCall.isCanceled();
    }

}