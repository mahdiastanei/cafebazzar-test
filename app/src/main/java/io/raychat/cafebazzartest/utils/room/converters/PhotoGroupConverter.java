package io.raychat.cafebazzartest.utils.room.converters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import io.raychat.cafebazzartest.model.explore.Group;
import io.raychat.cafebazzartest.model.explore.PhotoGroup;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class PhotoGroupConverter {
    @TypeConverter
    public static List<PhotoGroup> fromString(String value) {
        Type listType = new TypeToken<List<PhotoGroup>>() {
        }.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromArrayList(List<PhotoGroup> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }
}
