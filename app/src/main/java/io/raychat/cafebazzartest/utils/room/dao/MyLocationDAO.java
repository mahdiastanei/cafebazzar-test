package io.raychat.cafebazzartest.utils.room.dao;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import io.raychat.cafebazzartest.model.MyLocation;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@Dao
public interface MyLocationDAO {


    // FIND
    @Query("select * from myLocation")
    Single<MyLocation> getMyLocationSingle();

    @Query("select * from myLocation")
    Flowable<MyLocation> getMyLocationFlowable();

    // Observe
    @Query("select * from myLocation")
    LiveData<MyLocation> getMyLocationLiveData();

    @Query("select * from myLocation")
    MyLocation getMyLocation();

    //    @Query("select * from chats WHERE conversationId = :conversationId ORDER BY updateTimeStamp DESC LIMIT 1 ")
//    LiveData<ChatData> observeChatChange(String conversationId);
//
//    @Query("select * from chats WHERE conversationId = :conversationId ORDER BY updateTimeStamp DESC ")
//    List<ChatData> getAllChats(String conversationId);
//
//    @Query("SELECT * FROM chats where id = :chatId ")
//    ChatData getChat(String chatId);
//
//    @Query("select * from chats WHERE conversationId = :conversationId LIMIT 1")
//    Flowable<ChatData> getLastChat(String conversationId);
//
//    @Query("select * from chats WHERE conversationId = :conversationId AND type = :type")
//    List<ChatData> getImageChats(String conversationId, int type);
//
//    @Query("select COUNT(*) from chats WHERE conversationId = :conversationId")
//    int getChatCount(String conversationId);
//
//
//    //INSERT
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMyLocation(MyLocation myLocation);

    //
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    void insertChat(ChatData chat);
//
//
//    // UPDATE
    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateMyLocation(MyLocation myLocation);

    //
//    @Query("UPDATE chats SET message = :message , edited =  :isEdited WHERE id = :chatId")
//    void updateEditedMessage(String message, boolean isEdited, String chatId);
//
//    @Query("UPDATE chats SET type = :type WHERE id = :chatId")
//    void updateMessageType(int type, String chatId);
//
//
//    // DELETE
//    @Query("UPDATE chats SET message = :message WHERE id = :chatId")
//    void updateMessage(String message, String chatId);
//
//    @Query("DELETE FROM chats WHERE conversationId = :conversationId AND tmpId = :tmpId")
//    void deleteMessage(String conversationId, String tmpId);
//
//
    //CLEAR
    @Query("DELETE FROM myLocation")
    void clearTable();


}
