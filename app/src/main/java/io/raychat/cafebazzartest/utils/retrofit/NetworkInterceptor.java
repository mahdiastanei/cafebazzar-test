package io.raychat.cafebazzartest.utils.retrofit;

import android.content.Context;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class NetworkInterceptor implements Interceptor {

    /*
     * Step 1: We pass the context instance here,
     * since we need to get the ConnectivityStatus
     * to check if there is internet.
     * */
    private Context context;
    private NetworkEvent networkEvent;

    public NetworkInterceptor(Context context) {
        this.context = context;
        this.networkEvent = NetworkEvent.getInstance();
    }

    @NotNull
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        /*
         * Step 2: We check if there is internet
         * available in the device. If not, pass
         * the networkState as NO_INTERNET.
         * */

        if (!ConnectivityStatus.isConnected(context)) {
            networkEvent.publish(NetworkState.NO_INTERNET);
        } else {
            try {
                /*
                 * Step 3: Get the response code from the
                 * request. In this scenario we are only handling
                 * unauthorised and server unavailable error
                 * scenario
                 * */
                Response response = chain.proceed(request);
                if (response != null) {
                    switch (response.code()) {
                        case 401:
                            networkEvent.publish(NetworkState.UNAUTHORISED);
                            break;

                        case 503:
                        case 502:
                        case 404:
                            networkEvent.publish(NetworkState.NO_RESPONSE);
                            break;
                    }
                }
                return response;

            } catch (IOException e) {
                networkEvent.publish(NetworkState.NO_RESPONSE);
            }
        }
        return new Response.Builder()
                .code(404)
                .body(chain.proceed(request).body())
                .protocol(Protocol.HTTP_2)
                .message("Token has expired please login once again")
                .request(chain.request())
                .build();
    }
}
