package io.raychat.cafebazzartest.utils.retrofit;


import io.raychat.cafebazzartest.utils.constants.ApiConstants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public interface ApiResponseListener {
    void onResponseSuccess(String responseBodyString, ApiConstants.ApiFlags apiFlag);

    void onResponseError(Throwable throwable, ApiConstants.ApiFlags apiFlag);
}
