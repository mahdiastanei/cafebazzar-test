package io.raychat.cafebazzartest.utils.room.dataSource.explore;

import androidx.lifecycle.LiveData;

import io.raychat.cafebazzartest.model.explore.main.ExploreData;
import io.raychat.cafebazzartest.utils.room.dao.ExploreDAO;
import io.reactivex.Single;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class ExploreDataSource implements ExploreLocalSource {

    private final ExploreDAO exploreDAO;

    public ExploreDataSource(ExploreDAO exploreDAO) {
        this.exploreDAO = exploreDAO;
    }

    @Override
    public LiveData<ExploreData> getLastExploreData() {
        return exploreDAO.observeLastExploreData();
    }

    @Override
    public Single<ExploreData> getExploreData(int limit, int offset) {
        return exploreDAO.getExploreData(limit, offset);
    }

    @Override
    public void insertExploreData(ExploreData exploreData) {
        new Thread(() -> exploreDAO.insertExploreData(exploreData)).start();
    }

    @Override
    public void clear() {
        new Thread(exploreDAO::clearTable).start();
    }
}
