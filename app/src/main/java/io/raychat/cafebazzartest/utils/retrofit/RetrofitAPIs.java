package io.raychat.cafebazzartest.utils.retrofit;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public interface RetrofitAPIs {

    @Headers("Content-Type: application/json")
    @POST
    Call<ResponseBody> post(@Url String url, @Body RequestBody body);

    @GET
    Call<ResponseBody> get(@Url String url);

    @DELETE
    Call<ResponseBody> delete(@Url String url);

    @Headers("Content-Type: application/json")
    @PUT
    Call<ResponseBody> put(@Url String url, @Body RequestBody body);

}
