package io.raychat.cafebazzartest.utils.typeFace;

public class Typefaces {


    public final static String SAN_FRANCISCO = "fonts/SFProDisplay-Regular.ttf";
    public final static String SAN_FRANCISCO_BLACK = "fonts/SFProDisplay-Medium.ttf";

    public final static String[] FONTS = new String[]{SAN_FRANCISCO, SAN_FRANCISCO_BLACK};
}