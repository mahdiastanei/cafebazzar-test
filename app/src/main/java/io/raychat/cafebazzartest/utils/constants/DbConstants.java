package io.raychat.cafebazzartest.utils.constants;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class DbConstants {

    public static final String MAIN_DB_NAME = "MainDatabase";

    public interface TableNames {

        String META = "meta";
        String CATEGORY = "category";
        String GROUP = "group";
        String GROUP_ITEM = "groupItem";
        String ICON = "icon";
        String ITEM = "item";
        String LABELED_LAT_LNG = "labeledLatLng";
        String LOCATION = "location";
        String NE = "ne";
        String REASONS = "reason";
        String RESPONSE = "response";
        String SUGGESTED_BOUNDS = "suggestedBounds";
        String SW = "sw";
        String VENUE = "venue";
        String VENUE_PAGE = "venuePage";
        String WARNING = "warning";
        String MY_LOCATION = "myLocation";
        String EXPLORE = "explore";
        String PHOTOS = "photos";
        String PHOTO_GROUP = "PhotoGroup";
        String STATS = "stats";
        String HOURS = "hours";
        String TIPS = "tips";
        String USER = "user";
    }

    public interface Prefixes {
        String META = "meta_";
        String RESPONSE = "response_";
        String ICON = "icon_";
        String ITEMS = "icon_";
        String REASONS = "reason_";
        String VENUE = "venue_";
        String LABELED_LAT_LNGS = "labeledLatLngs_";
        String FORMATTED_ADDRESS = "formattedAddress_";
        String GROUP_ITEMS = "groupItem_";
        String WARNING = "warning_";
        String SUGGESTED_BOUNDS = "suggestedBounds_";
        String GROUPS = "groups_";
        String NE = "ne_";
        String SW = "sw_";
        String LOCATION = "location_";
        String CATEGORIES = "categories_";
        String VENUE_PAGE = "venuePages_";
        String PHOTOS = "photos_";
        String STATS = "stats_";
        String HOURS = "hours_";
        String USER = "user_";
        String TODO = "todo_";
        String LIKES = "likes_";
        String PHOTO = "photo_";
        String TIPS = "tips_";
    }

    public interface ColumnNames {

        interface Category {
            String ID = "id";
            String NAME = "name";
            String PLURAL_NAME = "pluralName";
            String SHORT_NAME = "shortName";
            String PRIMARY = "primary";
        }

        interface Group {
            String TYPE = "type";
            String NAME = "name";
            String ITEMS = "items";
        }

        interface Icon {
            String PREFIX = "prefix";
            String SUFFIX = "suffix";
        }

        interface Item {
            String SUMMARY = "summary";
            String TYPE = "type";
            String REASON_NAME = "reasonName";
        }

        interface LabeledLatLng {
            String LABEL = "label";
            String LAT = "lat";
            String LNG = "lng";
        }

        interface Location {
            String ADDRESS = "address";
            String CROSS_STREET = "crossStreet";
            String LAT = "lat";
            String LNG = "lng";
            String DISTANCE = "distance";
            String POSTAL_CODE = "postalCode";
            String CC = "cc";
            String CITY = "city";
            String STATE = "state";
            String COUNTRY = "country";
            String LABELED_LAT_LNGS = "labeledLatLngs";
            String FORMATTED_ADDRESS = "formattedAddress";
        }

        interface Meta {
            String CODE = "code";
            String REQUEST_ID = "requestId";
            String ERROR_TYPE = "errorType";
            String ERROR_DETAIL = "errorDetail";
        }

        interface Ne {
            String LAT = "lat";
            String LNG = "lng";
        }

        interface Reasons {
            String COUNT = "count";
            String ITEMS = "items";
        }

        interface Response {
            String SUGGESTED_BOUNDS = "suggestedBounds";
            String SUGGESTED_RADIUS = "suggestedRadius";
            String HEADER_LOCATION = "headerLocation";
            String HEADER_FULL_LOCATION = "headerFullLocation";
            String HEADER_LOCATION_GRANULARITY = "headerLocationGranularity";
            String TOTAL_RESULTS = "totalResults";
            String WARNING = "warning";
            String GROUPS = "groups";
        }

        interface Sw {
            String LAT = "lat";
            String LNG = "lng";
        }

        interface Venue {
            String ID = "id";
            String NAME = "name";
            String POPULARITY_BY_GEO = "popularityByGeo";
            String LOCATION = "location";
            String CATEGORIES = "categories";
            String VERIFIED = "verified";
            String RATING = "rating";
            String RATING_COLOR = "ratingColor";
            String RATING_SIGNALS = "ratingSignals";
        }

        interface VenuePage {
            String ID = "id";
        }

        interface Warning {
            String TEXT = "text";
        }

        public interface
        SuggestedBounds {
            String NE = "ne";
            String SW = "sw";
        }

        public interface MyLocation {
            String ID = "id";
            String LAT = "latitude";
            String LNG = "longitude";
            String DISTANCE = "distance";
            String SHOULD_REFRESH = "shouldRefresh";
        }

        public interface Photos {
            String COUNT = "count";
            String GROUPS = "groups";
        }

        public interface PhotoGroup {
            String ID = "id";
            String PREFIX = "prefix";
            String SUFFIX = "suffix";
        }

        public interface Stats {
            String TIP_COUNT = "tipCount";
            String USERS_COUNT = "usersCount";
            String CHECKINS_COUNT = "checkinsCount";
            String VISITS_COUNT = "visitsCount";
        }

        public interface Hours {
            String STATUS = "status";
            String IS_OPEN = "isOpen";
            String IS_LOCAL_HOLIDAY = "isLocalHoliday";
        }

        public interface Tips {
            String DIS_AGREE_COUNT = "disagreeCount";
            String AGREE_COUNT = "agreeCount";
            String LOG_VIEW = "logView";
            String PHOTO_URL = "photourl";
            String CANONICAL_URL = "canonicalUrl";
            String TYPE = "type";
            String TEXT = "text";
            String CREATED_AT = "createdAt";
            String ID = "id";
        }

        public interface GroupItem {
            String TIPS = "tips";
        }

        public interface User {
            String FIRST_NAME = "firstName";
            String LAST_NAME = "lastName";
            String COUNTRY_CODE = "countryCode";
        }
    }


}
