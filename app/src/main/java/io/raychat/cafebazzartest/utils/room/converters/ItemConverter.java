package io.raychat.cafebazzartest.utils.room.converters;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import io.raychat.cafebazzartest.model.explore.GroupItem;
import io.raychat.cafebazzartest.model.explore.Item;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class ItemConverter {
    @TypeConverter
    public static List<Item> fromString(String value) {
        Type listType = new TypeToken<List<Item>>() {
        }.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromArrayList(List<Item> list) {
        Gson gson = new Gson();
        return gson.toJson(list);
    }
}
