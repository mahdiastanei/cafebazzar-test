package io.raychat.cafebazzartest.utils.customView;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

import io.raychat.cafebazzartest.R;
import io.raychat.cafebazzartest.model.detail.Item;
import io.raychat.cafebazzartest.model.explore.PhotoGroup;

/**
 * A simple {@link Fragment} subclass.
 */
public class MySlider extends Fragment {

    private Item photoGroup;

    public MySlider(Item photoGroup) {
        this.photoGroup = photoGroup;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = null;
        try {
            if (photoGroup != null) {
                view = inflater.inflate(R.layout.simple_slider_layout, container, false);
                getSimpleSliderArgs(view, photoGroup);
            }
        } catch (Exception e) {
        }

        return view;
    }

    private void getSimpleSliderArgs(View view, Item photoGroup) {
        ImageView sliderImage = view.findViewById(R.id.imgSlider);
        Glide.with(sliderImage.getContext())
                .asBitmap()
                .load(photoGroup.getPhotoUrl("original"))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.img_explore_placeholder)
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        sliderImage.setImageBitmap(resource);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {

                    }
                });

    }
}
