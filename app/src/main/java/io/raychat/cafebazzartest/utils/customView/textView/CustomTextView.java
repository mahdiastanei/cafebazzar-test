package io.raychat.cafebazzartest.utils.customView.textView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;

import io.raychat.cafebazzartest.R;
import io.raychat.cafebazzartest.utils.AndroidUtilities;
import io.raychat.cafebazzartest.utils.typeFace.Typefaces;


/**
 * Created by Alireza on 2019-11-09.
 * This Class extends AppCompatTextView and sets custom fonts
 * and font size and ...
 * Declare new attrs in "custom_widget_attr.xml" in "values" directory
 */

public class CustomTextView extends AppCompatTextView {

    private static final String TAG = CustomTextView.class.getName();
    private int typefaceIndex;
    private float radius;
    private int bgColor;


    public CustomTextView(Context context) {
        super(context);
        init(context, null);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

        if (!isInEditMode() && attrs != null) {

            TypedArray properties = getContext().obtainStyledAttributes(attrs, R.styleable.CustomTextView);
            typefaceIndex = properties.getInt(R.styleable.CustomTextView_typeface, 0);
            radius = properties.getFloat(R.styleable.CustomTextView_customTextViewRadiusSize, 0);
            bgColor = properties.getInt(R.styleable.CustomTextView_customTextViewBGColor,
                    ContextCompat.getColor(context, android.R.color.transparent));
            int textSizeIndex = properties.getInt(R.styleable.CustomTextView_size, 5);
            properties.recycle();

            setTypeface(AndroidUtilities.getTypeface(Typefaces.FONTS[typefaceIndex]));

            switch (textSizeIndex) {
                case 1:
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 0.1f);
                    break;
                case 2:
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 0.3f);
                    break;
                case 3:
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 0.6f);
                    break;
                case 4:
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 0.8f);
                    break;
                case 5:
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 1.0f);
                    break;
                case 6:
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 1.2f);
                    break;
                case 7:
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 1.4f);
                    break;
                case 8:
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 1.6f);
                    break;
                case 9:
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 1.8f);
                    break;
                case 10:
                    setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 2.0f);
                    break;

            }

            if (radius != 0) {
                GradientDrawable shape = new GradientDrawable();
                shape.setCornerRadius(radius);
                shape.setColor(bgColor);
                setBackground(shape);
            } else if (bgColor != 0) {
                GradientDrawable shape = new GradientDrawable();
                shape.setColor(bgColor);
                setBackground(shape);
            }

        }
    }

    public void setTextSizeIndex(int textSizeIndex) {
        switch (textSizeIndex) {
            case 1:
                setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 0.4f);
                break;
            case 2:
                setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 0.6f);
                break;
            case 3:
                setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 0.8f);
                break;
            case 4:
                setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize());
                break;
            case 5:
                setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 1.4f);
                break;
            case 6:
                setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 1.6f);
                break;
            case 7:
                setTextSize(TypedValue.COMPLEX_UNIT_PX, getTextSize() * 1.8f);
                break;
        }

    }

    public void setTypefaceIndex(Context context, int typefaceIndex) {
        setTypeface(AndroidUtilities.getTypeface(Typefaces.FONTS[typefaceIndex]));
    }


}