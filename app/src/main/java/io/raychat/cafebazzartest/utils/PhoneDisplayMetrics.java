package io.raychat.cafebazzartest.utils;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.widget.FrameLayout;

public class PhoneDisplayMetrics {
    public static float getDisplayWidth(Activity activity) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int getImageMessageWidth(Activity activity) {
        int w = Math.round(getDisplayWidth(activity));
        return (w * 3) / 4 ;
    }

    public static DisplayMetrics getDisplayMetrics(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        return metrics;
    }
}
