package io.raychat.cafebazzartest.utils.room.dataSource.explore;

import androidx.lifecycle.LiveData;

import io.raychat.cafebazzartest.model.explore.main.ExploreData;
import io.reactivex.Single;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public interface ExploreLocalSource {

    LiveData<ExploreData> getLastExploreData();

    Single<ExploreData> getExploreData(int limit, int offset);

    void insertExploreData(ExploreData exploreData);

    void clear();
}
