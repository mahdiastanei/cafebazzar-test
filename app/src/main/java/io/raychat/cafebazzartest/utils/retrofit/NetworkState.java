package io.raychat.cafebazzartest.utils.retrofit;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public enum NetworkState {
    NO_INTERNET, NO_RESPONSE, UNAUTHORISED
}
