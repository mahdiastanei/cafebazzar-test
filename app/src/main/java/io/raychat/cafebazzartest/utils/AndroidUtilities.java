package io.raychat.cafebazzartest.utils;

import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;

import java.util.Hashtable;

import io.raychat.cafebazzartest.App;

/**
 * Created by Alireza Nezami on 4/9/2021.
 */
public class AndroidUtilities {
    private static final Hashtable<String, Typeface> typefaceCache = new Hashtable<>();

    public static Typeface getTypeface(String assetPath) {
        synchronized (typefaceCache) {
            if (!typefaceCache.containsKey(assetPath)) {
                try {
                    Typeface t;
                    if (Build.VERSION.SDK_INT >= 26) {
                        Typeface.Builder builder = new Typeface.Builder(App.getInstance().getAssets(), assetPath);
//                        if (assetPath.contains("medium")) {
//                            builder.setWeight(700);
//                        }
//                        if (assetPath.contains("italic")) {
//                            builder.setItalic(true);
//                        }
                        t = builder.build();
                    } else {
                        t = Typeface.createFromAsset(App.getInstance().getAssets(), assetPath);
                    }
                    typefaceCache.put(assetPath, t);
                } catch (Exception e) {
//                    if (BuildVars.LOGS_ENABLED) {
//                        FileLog.e("Could not get typeface '" + assetPath + "' because " + e.getMessage());
//                    }
                    return null;
                }
            }
            return typefaceCache.get(assetPath);
        }
    }

    public static double distanceBetweenTwoPoint(double srcLat, double srcLng, double desLat, double desLng) {
        double earthRadius = 3958.75;
        double dLat = Math.toRadians(desLat - srcLat);
        double dLng = Math.toRadians(desLng - srcLng);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(srcLat))
                * Math.cos(Math.toRadians(desLat)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;

        double meterConversion = 1609;
        return (int) (dist * meterConversion);
    }

}
