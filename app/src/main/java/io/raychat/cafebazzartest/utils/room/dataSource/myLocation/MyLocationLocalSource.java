package io.raychat.cafebazzartest.utils.room.dataSource.myLocation;

import androidx.lifecycle.LiveData;

import io.raychat.cafebazzartest.model.MyLocation;
import io.raychat.cafebazzartest.model.explore.main.ExploreData;
import io.reactivex.Flowable;
import io.reactivex.Single;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public interface MyLocationLocalSource {

    Single<MyLocation> getUserLastLocation();

    Flowable<MyLocation> getMyLocationFlowable();

    LiveData<MyLocation> getMyLocationLiveData();

    MyLocation getMyLocation();

    void insertMyLocation(MyLocation myLocation);

    void clear();
}
