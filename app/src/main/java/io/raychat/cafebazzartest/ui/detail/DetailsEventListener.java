package io.raychat.cafebazzartest.ui.detail;

/**
 * Created by Alireza Nezami on 4/10/2021.
 */
public interface DetailsEventListener {

    void loadMorePhotos(int offset);

    void loadMoreTips(int offset);
}
