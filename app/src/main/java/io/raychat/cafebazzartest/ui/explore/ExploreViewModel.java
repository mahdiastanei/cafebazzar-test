package io.raychat.cafebazzartest.ui.explore;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;

import io.raychat.cafebazzartest.model.MyLocation;
import io.raychat.cafebazzartest.model.explore.main.ExploreData;
import io.raychat.cafebazzartest.services.LocationUpdatesService;
import io.raychat.cafebazzartest.utils.Utils;
import io.raychat.cafebazzartest.utils.constants.ApiConstants;
import io.raychat.cafebazzartest.utils.constants.Constants;
import io.raychat.cafebazzartest.utils.location.LocationBroadcastReceiver;
import io.raychat.cafebazzartest.utils.retrofit.ApiResponseListener;
import io.raychat.cafebazzartest.utils.retrofit.NetworkService;
import io.raychat.cafebazzartest.utils.room.LocalDatabase;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
@SuppressLint("StaticFieldLeak")
public class ExploreViewModel extends AndroidViewModel implements
        SharedPreferences.OnSharedPreferenceChangeListener, ApiResponseListener {

    private static final String TAG = ExploreViewModel.class.getSimpleName();

    private final Activity activity;

    private final ExploreEventListener eventListener;

    // The BroadcastReceiver used to listen from broadcasts from the service.
    private final LocationBroadcastReceiver broadcastReceiver;

    private final MediatorLiveData<ExploreData> result = new MediatorLiveData<>();

    private final MutableLiveData<ExploreData> exploreLiveData;
    private final MutableLiveData<MyLocation> myLocationLiveData;


    // A reference to the service used to get location updates.
    private LocationUpdatesService mService = null;

    // Tracks the bound state of the service.
    private boolean mBound = false;

    // Monitors the state of the connection to the service.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationUpdatesService.LocalBinder binder = (LocationUpdatesService.LocalBinder) service;
            mService = binder.getService();
            if (!Utils.checkPermissions(activity)) {
                if (eventListener != null)
                    eventListener.requestLocationPermission();
            } else {
                mService.requestLocationUpdates();
            }
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
            mBound = false;
        }
    };
    private final Observer<ExploreData> onExploreDataChange = new Observer<ExploreData>() {
        @Override
        public void onChanged(ExploreData exploreData) {
            exploreLiveData.postValue(exploreData);
        }
    };
    private final Observer<MyLocation> onMyLocationChanged = new Observer<MyLocation>() {
        @Override
        public void onChanged(MyLocation myLocation) {
            myLocationLiveData.postValue(myLocation);
        }
    };

    public ExploreViewModel(@NonNull Application application, Activity activity,
                            ExploreEventListener eventListener) {
        super(application);
        this.activity = activity;
        this.eventListener = eventListener;

        exploreLiveData = new MutableLiveData<>();
        myLocationLiveData = new MutableLiveData<>();
        broadcastReceiver = new LocationBroadcastReceiver();
        broadcastReceiver.setEventListener(eventListener);
        PreferenceManager.getDefaultSharedPreferences(activity)
                .registerOnSharedPreferenceChangeListener(this);

        // Check that the user hasn't revoked permissions by going to Settings.
        if (Utils.requestingLocationUpdates(activity)) {
            if (!Utils.checkPermissions(activity)) {
                if (eventListener != null)
                    eventListener.requestLocationPermission();
            }
        }

        LiveData<ExploreData> exploreDataLiveData = LocalDatabase.exploreDataSource.getLastExploreData();
        LiveData<MyLocation> myLocationLiveData = LocalDatabase.myLocationDataSource.getMyLocationLiveData();

        result.addSource(exploreDataLiveData, onExploreDataChange);
        result.addSource(myLocationLiveData, onMyLocationChanged);
    }


    public void bindService() {
        // Bind to the service. If the service is in foreground mode, this signals to the service
        // that since this activity is in the foreground, the service can exit foreground mode.
        activity.bindService(new Intent(activity, LocationUpdatesService.class), mServiceConnection,
                Context.BIND_AUTO_CREATE);

    }

    public void requestLocationUpdates() {
        if (mService != null)
            mService.requestLocationUpdates();
    }

    public void registerReceiver() {
        LocalBroadcastManager.getInstance(activity).registerReceiver(broadcastReceiver,
                new IntentFilter(LocationUpdatesService.ACTION_BROADCAST));
    }

    public void unregisterReceiver() {
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(broadcastReceiver);
    }

    public void unbindService() {
        if (mBound) {
            // Unbind from the service when the activity dies.
            activity.unbindService(mServiceConnection);
            mBound = false;
        }
        PreferenceManager.getDefaultSharedPreferences(activity)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    public void getNewExploreData(int offset, MyLocation location) {
        ApiConstants.ApiFlags flag;
        if (offset > 0) {
            flag = ApiConstants.ApiFlags.OFFSET_EXPLORE;
        } else {
            flag = ApiConstants.ApiFlags.NEW_EXPLORE;
        }
        new NetworkService(activity)
                .setUrl(ApiConstants.ApiUrls.EXPLORE)
                .addParameter(ApiConstants.Parameters.CLIENT_ID, Constants.CLIENT_ID)
                .addParameter(ApiConstants.Parameters.CLIENT_SECRET, Constants.CLIENT_SECRET)
                .addParameter(ApiConstants.Parameters.V, Constants.V)
                .addParameter(ApiConstants.Parameters.LOCATION, location.getLocation())
//                .addParameter(ApiConstants.Parameters.LOCATION, "40.8099438,-74.1714186")
                .addParameter(ApiConstants.Parameters.LIMIT, ApiConstants.ParameterDefaultValues.limit)
                .addParameter(ApiConstants.Parameters.OFFSET, offset)
                .addParameter(ApiConstants.Parameters.SORT_BY_DISTANCE, ApiConstants.ParameterDefaultValues.sortByDistance)
                .addParameter(ApiConstants.Parameters.RADIUS, 1000)
                .setApiFlag(flag)
                .setApiResponseListener(this)
                .get();
    }

    public MutableLiveData<ExploreData> getExploreLiveData() {
        return exploreLiveData;
    }

    public MutableLiveData<MyLocation> getMyLocationLiveData() {
        return myLocationLiveData;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(Utils.KEY_NEAR_ME_ENABLED)) {
            if (eventListener != null)
                eventListener.setButtonState(sharedPreferences.getBoolean(Utils.KEY_NEAR_ME_ENABLED,
                        false));
        }
    }

    @Override
    public void onResponseSuccess(String responseBodyString, ApiConstants.ApiFlags apiFlag) {
        switch (apiFlag) {
            case NEW_EXPLORE:
                if (!responseBodyString.isEmpty()) {
                    ExploreData exploreData = new Gson().fromJson(responseBodyString, ExploreData.class);
                    if (exploreData != null) {
                        eventListener.clearExploreDate();
                        Log.i(TAG, "onChanged: Room + clear explore");
                        LocalDatabase.exploreDataSource.clear();
                        Log.i(TAG, "onChanged: Room + insert explore");
                        new Handler().postDelayed(() -> LocalDatabase.exploreDataSource.insertExploreData(exploreData), 1000);
                    }
                }
                break;
            case OFFSET_EXPLORE:
                if (!responseBodyString.isEmpty()) {
                    ExploreData exploreData = new Gson().fromJson(responseBodyString, ExploreData.class);
                    if (exploreData != null) {
                        LocalDatabase.exploreDataSource.insertExploreData(exploreData);
                    }
                }
                break;
        }
    }

    @Override
    public void onResponseError(Throwable throwable, ApiConstants.ApiFlags apiFlag) {

    }
}
