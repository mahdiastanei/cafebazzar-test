package io.raychat.cafebazzartest.ui.detail;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import io.raychat.cafebazzartest.R;
import io.raychat.cafebazzartest.adapter.ViewPager2Adapter;
import io.raychat.cafebazzartest.adapter.ViewPagerAdapter;
import io.raychat.cafebazzartest.databinding.ActivityDetailsBinding;
import io.raychat.cafebazzartest.model.detail.Item;
import io.raychat.cafebazzartest.model.detail.Photos;
import io.raychat.cafebazzartest.model.detail.Venue;
import io.raychat.cafebazzartest.ui.ViewModelFactory;
import io.raychat.cafebazzartest.utils.constants.Constants;
import io.raychat.cafebazzartest.utils.customView.MySlider;
import io.raychat.cafebazzartest.utils.customView.textView.CustomTextView;

public class DetailsActivity extends AppCompatActivity implements DetailsEventListener {

    //ViewModel for details activity
    DetailsViewModel viewModel;
    // View binding of this activity
    ActivityDetailsBinding binding;

    // venue id that want to get it's details
    private String venueId;

    // Runnable for changing the slider with an interval
    private Runnable runnable;

    // Venue Info's ViewPagerAdapter
    private ViewPager2Adapter infoViewPagerAdapter;

    private final Observer<? super Venue> onDetailsReceived = new Observer<io.raychat.cafebazzartest.model.detail.Venue>() {
        @Override
        public void onChanged(Venue venue) {
            initView(venue);
        }
    };

    private final Observer<? super Photos> onPhotosReceived = new Observer<Photos>() {
        @Override
        public void onChanged(Photos photos) {
            if (photos != null) {
                infoViewPagerAdapter.updatePhotosFragment(photos);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(DetailsActivity.this,
                new ViewModelFactory(DetailsActivity.this.getApplication(), DetailsActivity.this))
                .get(DetailsViewModel.class);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Bundle bundle = getIntent().getExtras();
//        if (bundle != null && bundle.containsKey(Constants.IntentConstants.VENUE_DATA)) {
//            GroupItem venueData = new Gson().fromJson(bundle.getString(Constants.IntentConstants.VENUE_DATA), GroupItem.class);
//            if (venueData == null) {
//                finish();
//            }
//        }
        if (bundle != null && bundle.containsKey(Constants.IntentConstants.VENUE_ID)) {
            venueId = bundle.getString(Constants.IntentConstants.VENUE_ID);
        } else {
            venueId = "54033d82498e6650fca8f377";
            finish();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (viewModel != null) {
            viewModel.getVenueLiveData().observe(DetailsActivity.this, onDetailsReceived);
            viewModel.getPhotosLiveData().observe(DetailsActivity.this, onPhotosReceived);
            viewModel.getVenueDetail(venueId);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    private void initView(Venue venue) {
        setupSlider(venue);
        showName(venue);
        showStats(venue);
        setupDetailsViewPager(venue);

    }

    private void setupDetailsViewPager(Venue venue) {
        infoViewPagerAdapter = new ViewPager2Adapter(getSupportFragmentManager(), new Lifecycle() {
            @Override
            public void addObserver(@NonNull LifecycleObserver observer) {

            }

            @Override
            public void removeObserver(@NonNull LifecycleObserver observer) {

            }

            @NonNull
            @Override
            public State getCurrentState() {
                return null;
            }
        });
        List<Fragment> fragmentList = new ArrayList<>();
        if (venue.getPhotos() != null)
            fragmentList.add(new PhotosFragment(venue.getPhotos()));
        else
            fragmentList.add(new PhotosFragment());
        if (venue.getTips() != null && venue.getTips().getTipGroups() != null)
            fragmentList.add(new TipsFragment(venue.getTips().getTipGroups()));
        else
            fragmentList.add(new TipsFragment());
        infoViewPagerAdapter.setFragmentList(fragmentList);
        binding.includeDetails.viewPageDetails.setAdapter(infoViewPagerAdapter);
        binding.includeDetails.viewPageDetails.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        binding.includeDetails.viewPageDetails.setOffscreenPageLimit(3);
        binding.includeDetails.viewPageDetails.setUserInputEnabled(false);
        binding.includeDetails.viewPageDetails.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
            }
        });
//        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayoutUserDetail));
        setupTabLayout();
    }

    private void setupTabLayout() {
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(binding.includeDetails.tabLayoutDetails, binding.includeDetails.viewPageDetails, true,
                (tab, position) -> {
                    tab.select();
                    binding.includeDetails.viewPageDetails.setCurrentItem(tab.getPosition());
                });
        tabLayoutMediator.attach();
        String[] tabTitles = getResources().getStringArray(R.array.detailsTabTitles);
//        tabLayoutUserDetail.setupWithViewPager(viewPager);
//        tabLayoutUserDetail.addOnTabSelectedListener(onTabSelectedListener(viewPager));
        for (int i = 0; i < binding.includeDetails.tabLayoutDetails.getTabCount(); i++) {
            View view = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            CustomTextView txtTabTitle = view.findViewById(R.id.tab_title);
            txtTabTitle.setText(tabTitles[i]);
            txtTabTitle.setGravity(View.TEXT_ALIGNMENT_CENTER);
            binding.includeDetails.tabLayoutDetails.getTabAt(i).setCustomView(view);
        }
    }

    private void showName(Venue venue) {
        binding.txtName.setText(venue.getName());
        if (venue.isVerified()) {
            binding.txtName.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    ResourcesCompat.getDrawable(getResources(), R.drawable.ic_verified, null),
                    null);
        }
    }

    private void showStats(Venue venue) {
        showRating(venue);
        showCheckIns(venue);
        showPrice(venue);
        showCategories(venue);

        binding.shimmerStats.hideShimmer();
    }

    private void showCategories(Venue venue) {
        StringBuilder categoryNames = new StringBuilder();
        for (int i = 0; i < venue.getCategories().size(); i++) {
            categoryNames
                    .append(venue.getCategories().get(i).getName())
                    .append(i < venue.getCategories().size() - 1 ? " | " : "");
        }
        binding.txtCategoryIsOpen.setText(categoryNames);
    }

    private void showPrice(Venue venue) {

//        if (venue.getAttributes() != null &&
//                venue.getAttributes().getGroups() != null &&
//                venue.getAttributes().getGroups().size() > 1 &&
//                venue.getAttributes().getGroups().get(0).getType().equals("price")) {
//            binding.txtPrice.setText(venue.getAttributes().getGroups().get(0).getItems().get(0).getDisplayValue());
//        }
    }

    private void showCheckIns(Venue venue) {
        if (venue.getStats() != null) {
            binding.txtCheckIns.setText(venue.getStats().getCheckinsCount());
        }

    }

    private void showRating(Venue venue) {
        if (venue.getRating() != null) {
            binding.txtRating.setText(String.format(Locale.getDefault(), "%s / %d", venue.getRating(), 10));
            binding.txtRating.setTextColor(Color.parseColor("#" + venue.getRatingColor()));
            binding.txtTotalVotes.setText(String.format("%s ratings", venue.getRatingSignals()));
            Drawable drawable = applyThemeToDrawable(
                    ResourcesCompat.getDrawable(getResources(), R.drawable.ic_like, null),
                    venue.getRatingColor());
            binding.txtRating.setCompoundDrawablesWithIntrinsicBounds(null,
                    null,
                    drawable,
                    null
            );
        }
    }

    private void setupSlider(Venue venue) {
        if (venue.getPhotos() != null &&
                venue.getPhotos().getGroups() != null &&
                venue.getPhotos().getGroups().size() > 0) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    (int) (Math.round(binding.lytSlider.getWidth()) / 1.5f));
            binding.lytSlider.setLayoutParams(params);

            binding.viewPagerSlider.setAdapter(addSlides(venue.getPhotos().getVenuePhotos().getItems()));
            if (binding.viewPagerSlider.getAdapter() != null)
                binding.viewPagerSlider.setCurrentItem(0);

            binding.sliderIndicator.setViewPager(binding.viewPagerSlider);
            try {
                Handler handler = new Handler();
                runnable = () -> {
                    int currentPos = binding.viewPagerSlider.getCurrentItem();
                    int nextPos;
                    if (currentPos == binding.viewPagerSlider.getAdapter().getCount() - 1) {
                        nextPos = 0;
                    } else {
                        nextPos = binding.viewPagerSlider.getCurrentItem() + 1;
                    }
                    binding.viewPagerSlider.setCurrentItem(nextPos);
                    handler.postDelayed(runnable, 3000);
                };
                handler.post(runnable);
            } catch (Exception e) {

            }
        }

    }

    private ViewPagerAdapter addSlides(List<Item> list) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (Item slider : list) {
            viewPagerAdapter.addFragment(new MySlider(slider));
        }
        return viewPagerAdapter;
    }

    public Drawable applyThemeToDrawable(Drawable image, String ratingColor) {
        if (image != null) {
            PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(Color.parseColor("#" + ratingColor),
                    PorterDuff.Mode.SRC_ATOP);

            image.setColorFilter(porterDuffColorFilter);
        }
        return image;
    }


    @Override
    public void loadMorePhotos(int offset) {
        Log.i("TAG", "loadMorePhotos: " + offset);
        viewModel.loadMorePhotos(offset, venueId);
    }

    @Override
    public void loadMoreTips(int offset) {
        viewModel.loadMoreTips(offset, venueId);
    }
}