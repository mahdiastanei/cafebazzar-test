package io.raychat.cafebazzartest.ui.detail;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;

import io.raychat.cafebazzartest.model.detail.Photos;
import io.raychat.cafebazzartest.model.detail.Venue;
import io.raychat.cafebazzartest.model.detail.main.VenueData;
import io.raychat.cafebazzartest.ui.explore.ExploreViewModel;
import io.raychat.cafebazzartest.utils.constants.ApiConstants;
import io.raychat.cafebazzartest.utils.constants.Constants;
import io.raychat.cafebazzartest.utils.retrofit.ApiResponseListener;
import io.raychat.cafebazzartest.utils.retrofit.NetworkService;

/**
 * Created by Alireza Nezami on 4/8/2021.
 */
@SuppressLint("StaticFieldLeak")
public class DetailsViewModel extends AndroidViewModel implements ApiResponseListener {

    private static final String TAG = ExploreViewModel.class.getSimpleName();

    private final Activity activity;

    // LiveData for observing venue detail object
    MutableLiveData<Venue> venueLiveData;

    //LiveData for posting and observing photos
    MutableLiveData<Photos> photosLiveData;

    public DetailsViewModel(@NonNull Application application, Activity activity) {
        super(application);
        this.activity = activity;

        venueLiveData = new MutableLiveData<io.raychat.cafebazzartest.model.detail.Venue>();
        photosLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<io.raychat.cafebazzartest.model.detail.Venue> getVenueLiveData() {
        return venueLiveData;
    }

    public void getVenueDetail(String venueId) {
        new NetworkService(activity)
                .setUrl(ApiConstants.ApiUrls.VENUES)
                .setQuery(venueId)
//                .setQuery("49eeaf08f964a52078681fe3")
                .addParameter(ApiConstants.Parameters.CLIENT_ID, Constants.CLIENT_ID)
                .addParameter(ApiConstants.Parameters.CLIENT_SECRET, Constants.CLIENT_SECRET)
                .addParameter(ApiConstants.Parameters.V, Constants.V)
                .setApiFlag(ApiConstants.ApiFlags.VENUE_DETAIL)
                .setApiResponseListener(this)
                .get();
    }

    public MutableLiveData<Photos> getPhotosLiveData() {
        return photosLiveData;
    }

    public void loadMorePhotos(int offset, String venueId) {
        new NetworkService(activity)
                .setUrl(ApiConstants.ApiUrls.VENUES)
                .setQuery(venueId + ApiConstants.ApiUrls.PHOTOS)
                .addParameter(ApiConstants.Parameters.CLIENT_ID, Constants.CLIENT_ID)
                .addParameter(ApiConstants.Parameters.CLIENT_SECRET, Constants.CLIENT_SECRET)
                .addParameter(ApiConstants.Parameters.V, Constants.V)
                .addParameter(ApiConstants.Parameters.LIMIT, ApiConstants.ParameterDefaultValues.limit)
                .addParameter(ApiConstants.Parameters.OFFSET, offset)
                .setApiFlag(ApiConstants.ApiFlags.PHOTOS)
                .setApiResponseListener(this)
                .get();
    }

    public void loadMoreTips(int offset, String venueId) {
        new NetworkService(activity)
                .setUrl(ApiConstants.ApiUrls.VENUES)
                .setQuery(venueId + ApiConstants.ApiUrls.PHOTOS)
                .addParameter(ApiConstants.Parameters.CLIENT_ID, Constants.CLIENT_ID)
                .addParameter(ApiConstants.Parameters.CLIENT_SECRET, Constants.CLIENT_SECRET)
                .addParameter(ApiConstants.Parameters.V, Constants.V)
                .addParameter(ApiConstants.Parameters.LIMIT, ApiConstants.ParameterDefaultValues.limit)
                .addParameter(ApiConstants.Parameters.OFFSET, offset)
                .setApiFlag(ApiConstants.ApiFlags.PHOTOS)
                .setApiResponseListener(this)
                .get();
    }

    @Override
    public void onResponseSuccess(String responseBodyString, ApiConstants.ApiFlags apiFlag) {
        if (apiFlag == ApiConstants.ApiFlags.VENUE_DETAIL) {
            if (!responseBodyString.isEmpty()) {
                VenueData venueData = new Gson().fromJson(responseBodyString, VenueData.class);
                if (venueData != null && venueData.getResponse() != null && venueData.getResponse().getVenue() != null) {
                    venueLiveData.postValue(venueData.getResponse().getVenue());
                }
            }
        } else if (apiFlag == ApiConstants.ApiFlags.PHOTOS) {
            if (!responseBodyString.isEmpty()) {
                VenueData venueData = new Gson().fromJson(responseBodyString, VenueData.class);
                if (venueData != null && venueData.getResponse() != null && venueData.getResponse().getPhotos() != null) {
                    photosLiveData.postValue(venueData.getResponse().getPhotos());
                }

            }
        }
    }

    @Override
    public void onResponseError(Throwable throwable, ApiConstants.ApiFlags apiFlag) {

    }

}
