package io.raychat.cafebazzartest.ui;

import android.app.Activity;
import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import io.raychat.cafebazzartest.ui.detail.DetailsViewModel;
import io.raychat.cafebazzartest.ui.explore.ExploreEventListener;
import io.raychat.cafebazzartest.ui.explore.ExploreViewModel;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public class ViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    @NonNull
    private final Application application;
    private final  Activity activity;
    private ExploreEventListener eventListener;

    public ViewModelFactory(@NonNull Application application, Activity activity) {
        this.application = application;
        this.activity = activity;
    }

    public ViewModelFactory setEventListener(ExploreEventListener eventListener) {
        this.eventListener = eventListener;
        return this;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass == ExploreViewModel.class) {
            return (T) new ExploreViewModel(application,activity, eventListener);
        }
        else if (modelClass == DetailsViewModel.class) {
            return (T) new DetailsViewModel(application,activity);
        }
        return null;
    }
}
