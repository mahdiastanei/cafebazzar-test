package io.raychat.cafebazzartest.ui.explore;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import io.raychat.cafebazzartest.BuildConfig;
import io.raychat.cafebazzartest.R;
import io.raychat.cafebazzartest.adapter.BaseAdapter;
import io.raychat.cafebazzartest.adapter.VenuesAdapter;
import io.raychat.cafebazzartest.databinding.ActivityExploreBinding;
import io.raychat.cafebazzartest.model.MyLocation;
import io.raychat.cafebazzartest.model.explore.GroupItem;
import io.raychat.cafebazzartest.model.explore.main.ExploreData;
import io.raychat.cafebazzartest.ui.ViewModelFactory;
import io.raychat.cafebazzartest.ui.detail.DetailsActivity;
import io.raychat.cafebazzartest.utils.Utils;
import io.raychat.cafebazzartest.utils.constants.ApiConstants;
import io.raychat.cafebazzartest.utils.constants.Constants;
import io.raychat.cafebazzartest.utils.customView.MyRecyclerViewScrollListener;
import io.raychat.cafebazzartest.utils.room.LocalDatabase;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

@SuppressLint("StaticFieldLeak")
public class ExploreActivity extends AppCompatActivity {

    private static final String TAG = ExploreActivity.class.getSimpleName();

    // code for requesting runtime location permissions
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 586;
    // Auto-Generated Class for this activity's xml file
    private ActivityExploreBinding binding;
    // Venues Adapter
    private VenuesAdapter adapter;

    public ExploreViewModel viewModel;

    private String SETTINGS_BACK_STACK = "settings";

    private final ExploreEventListener exploreEventListener = new ExploreEventListener() {
        @Override
        public void requestLocationPermission() {
            requestPermissions();
        }

        @Override
        public void setButtonState(boolean isLocationRequested) {
            setSwitchState(isLocationRequested);
        }

        @Override
        public void clearExploreDate() {
            if (adapter!=null)
                adapter.clear();
        }

    };
    private final BaseAdapter.OnItemClickListener onVenuesItemClickListener = new BaseAdapter.OnItemClickListener() {
        @Override
        public <T> void onItemClick(T item) {
            if (item instanceof GroupItem) {
                startActivity(new Intent(ExploreActivity.this, DetailsActivity.class)
                        .putExtra(Constants.IntentConstants.VENUE_ID, ((GroupItem) item).getVenue().getId()));
            }
        }
    };

    private final Observer<? super MyLocation> onMyLocationChanged = (Observer<MyLocation>) myLocation -> {
        if (myLocation != null && myLocation.shouldRefresh()) {
            getExploreData(adapter != null ? adapter.getItemCount() : 0, myLocation);
        }
    };
    private final Observer<? super ExploreData> onExploreDataAdded = new Observer<ExploreData>() {
        @Override
        public void onChanged(ExploreData exploreData) {
            if (exploreData != null) {
                binding.loading.setVisibility(View.GONE);
                adapter.addAll(exploreData.getResponse().getRecommendedGroups().getGroupItems());
            }
        }
    };
    private final FragmentEventListener onFragmentEventListener = new FragmentEventListener() {
        @Override
        public <T> void onItemClick(MyLocation item) {
            if (viewModel != null) {
                viewModel.getNewExploreData(0, item);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityExploreBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        viewModel = new ViewModelProvider(ExploreActivity.this,
                new ViewModelFactory(ExploreActivity.this.getApplication(),
                        ExploreActivity.this)
                        .setEventListener(exploreEventListener))
                .get(ExploreViewModel.class);

        initRecyclerView();

    }

    @Override
    protected void onStart() {
        super.onStart();

        binding.lythNearMe.setOnClickListener(view -> {
            if (viewModel != null) {
                viewModel.bindService();
                binding.switchNearMe.setChecked(!binding.switchNearMe.isChecked());
            }
        });

        binding.btnSettings.setOnClickListener(v -> showSettings());
        // Restore the state of the buttons when the activity (re)launches.
//        setSwitchState(Utils.requestingLocationUpdates(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (viewModel != null) {
            viewModel.registerReceiver();
            LocalDatabase.exploreDataSource.getLastExploreData().observe(ExploreActivity.this, onExploreDataAdded);
            LocalDatabase.myLocationDataSource.getMyLocationLiveData().observeForever(onMyLocationChanged);
        }
    }

    @Override
    protected void onPause() {
        if (viewModel != null)
            viewModel.unregisterReceiver();

        super.onPause();
    }

    @Override
    protected void onStop() {
        if (viewModel != null)
            viewModel.unbindService();

        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            binding.fragmentContainer.setVisibility(View.GONE);
        } else {
            super.onBackPressed();
        }
    }

    private void setSwitchState(boolean requestingLocationUpdates) {
        binding.switchNearMe.setChecked(requestingLocationUpdates);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            Snackbar.make(
                    findViewById(binding.getRoot().getId()),
                    R.string.permission_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, view -> {
                        // Request permission
                        ActivityCompat.requestPermissions(ExploreActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_PERMISSIONS_REQUEST_CODE);
                    })
                    .show();
        } else {
            Log.i(TAG, "Requesting permission");
            String[] permissions = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            if (Utils.checkPermissions(ExploreActivity.this)) {
                checkLocationStatus();
            } else {
                ActivityCompat.requestPermissions(ExploreActivity.this,
                        permissions,
                        REQUEST_PERMISSIONS_REQUEST_CODE);
            }
        }
    }

    public void checkLocationStatus() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }
    }

    private void initRecyclerView() {
        if (binding != null) {
            binding.venuesRecycler.setLayoutManager(new LinearLayoutManager(ExploreActivity.this));
            adapter = new VenuesAdapter(ExploreActivity.this);
            adapter.setOnItemClickListener(onVenuesItemClickListener);
            binding.venuesRecycler.setAdapter(adapter);

            MyRecyclerViewScrollListener recyclerViewScrollListener = new MyRecyclerViewScrollListener((LinearLayoutManager) binding.venuesRecycler.getLayoutManager()) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    if (totalItemsCount >= ApiConstants.ParameterDefaultValues.limit - 1) {
                        getExploreDataFromCache(1, page);
                    }
                }
            };

            binding.venuesRecycler.addOnScrollListener(recyclerViewScrollListener);
        }
    }

    private void getExploreData(int offset, MyLocation location) {
        if (viewModel != null) {
            if (location == null) {
                LocalDatabase.myLocationDataSource.getUserLastLocation()
                        .subscribeOn(Schedulers.computation())
                        .subscribe(new SingleObserver<MyLocation>() {
                            @Override
                            public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {

                            }

                            @Override
                            public void onSuccess(@io.reactivex.annotations.NonNull MyLocation myLocation) {
                                Log.i(TAG, "onChanged: Room + onSuccess MyLocation");
                                new Handler(Looper.getMainLooper()).post(() -> binding.loading.setVisibility(View.VISIBLE));
                                viewModel.getNewExploreData(offset, myLocation);
                            }

                            @Override
                            public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                                new Handler(Looper.getMainLooper()).post(() -> binding.loading.setVisibility(View.VISIBLE));
                                viewModel.getNewExploreData(offset, null);
                            }
                        });
            } else {
                new Handler(Looper.getMainLooper()).post(() -> binding.loading.setVisibility(View.VISIBLE));
                viewModel.getNewExploreData(offset, location);
            }
        }
    }

    private void getExploreDataFromCache(int limit, int offset) {
        LocalDatabase.exploreDataSource.getExploreData(limit, offset)
                .subscribeOn(Schedulers.computation())
                .subscribe(new SingleObserver<ExploreData>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                    }

                    @Override
                    public void onSuccess(@io.reactivex.annotations.NonNull ExploreData exploreData) {
                        new Handler(Looper.getMainLooper()).post(() -> {
                            binding.loading.setVisibility(View.GONE);
                            adapter.addAll(exploreData.getResponse().getRecommendedGroups().getGroupItems());
                        });
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull Throwable e) {
                        Log.i(TAG, "onError: getExploreData " + e.toString());
                        getExploreData(adapter.getItemCount(), null);
                    }
                });
    }

    private void showSettings() {
        SettingsFragment fragment = new SettingsFragment(onFragmentEventListener);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .replace(R.id.fragmentContainer, fragment)
                .addToBackStack(SETTINGS_BACK_STACK)
                .commit();
        binding.fragmentContainer.setVisibility(View.VISIBLE);
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted.
                checkLocationStatus();
                if (viewModel != null)
                    viewModel.requestLocationUpdates();
            } else {
                // Permission denied.
                setSwitchState(false);
                Snackbar.make(
                        findViewById(binding.getRoot().getId()),
                        R.string.permission_denied_explanation,
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        })
                        .show();
            }
        }
    }


}