package io.raychat.cafebazzartest.ui.explore;

import io.raychat.cafebazzartest.model.MyLocation;

/**
 * Created by Alireza Nezami on 4/13/2021.
 */
public interface FragmentEventListener {
    <T> void onItemClick(MyLocation item);
}