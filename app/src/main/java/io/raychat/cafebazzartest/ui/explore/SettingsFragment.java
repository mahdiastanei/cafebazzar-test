package io.raychat.cafebazzartest.ui.explore;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import io.raychat.cafebazzartest.databinding.FragmentSettingsBinding;
import io.raychat.cafebazzartest.model.MyLocation;
import io.raychat.cafebazzartest.services.LocationUpdatesService;
import io.raychat.cafebazzartest.utils.constants.Constants;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class SettingsFragment extends Fragment {

    FragmentSettingsBinding binding;

    private FragmentEventListener eventListener;
    private Context context;

    public SettingsFragment(FragmentEventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSettingsBinding.inflate(inflater);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int distance = PreferenceManager.getDefaultSharedPreferences(context).getInt(Constants.DISTANCE_TO_REFRESH_VALUE,
                LocationUpdatesService.DISTANCE_TO_REFRESH);
        Long interval = PreferenceManager.getDefaultSharedPreferences(context).getLong(Constants.UPDATE_INTERVAL_VALUE,
                LocationUpdatesService.UPDATE_INTERVAL_IN_MILLISECONDS);

        binding.edtDistance.setText(String.valueOf(distance));
        binding.edtInterval.setText(String.valueOf(interval));

        binding.include.btnSave.setOnClickListener(v -> {
            PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(Constants.DISTANCE_TO_REFRESH_VALUE,
                    Integer.parseInt(binding.edtDistance.getText().toString())).apply();

            PreferenceManager.getDefaultSharedPreferences(context).edit().putLong(Constants.UPDATE_INTERVAL_VALUE,
                    Long.parseLong(binding.edtInterval.getText().toString())).apply();

            LocationUpdatesService.DISTANCE_TO_REFRESH = Integer.parseInt(binding.edtDistance.getText().toString());
            LocationUpdatesService.UPDATE_INTERVAL_IN_MILLISECONDS = Integer.parseInt(binding.edtInterval.getText().toString());
            getActivity().onBackPressed();

        });
        binding.include.btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        binding.txtNewYork.setOnClickListener(v -> {
            if (eventListener != null)
                eventListener.onItemClick(new MyLocation().setLatitude(40.711406).setLongitude(-73.9156235));

            getActivity().onBackPressed();
        });
        binding.txtSydney.setOnClickListener(v -> {
            if (eventListener != null)
                eventListener.onItemClick(new MyLocation().setLatitude(-33.8760403).setLongitude(151.073849));

            getActivity().onBackPressed();
        });
        binding.txtLondon.setOnClickListener(v -> {
            if (eventListener != null)
                eventListener.onItemClick(new MyLocation().setLatitude(51.5104923).setLongitude(-0.1439844));

            getActivity().onBackPressed();
        });
        binding.txtTehran.setOnClickListener(v -> {
            if (eventListener != null)
                eventListener.onItemClick(new MyLocation().setLatitude(35.7347937).setLongitude(51.3722163));

            getActivity().onBackPressed();
        });
    }
}