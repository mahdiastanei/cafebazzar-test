package io.raychat.cafebazzartest.ui.detail;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.raychat.cafebazzartest.adapter.TipsAdapter;
import io.raychat.cafebazzartest.databinding.FragmentPhotosBinding;
import io.raychat.cafebazzartest.model.detail.Group;
import io.raychat.cafebazzartest.model.detail.Photos;
import io.raychat.cafebazzartest.utils.PhoneDisplayMetrics;
import io.raychat.cafebazzartest.utils.customView.MyRecyclerViewScrollListener;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class TipsFragment extends Fragment {

    private FragmentPhotosBinding binding;

    private List<Group> groups;
    private Context context;
    private TipsAdapter adapter;

    // interface for communicate with activity
    private DetailsEventListener detailsEventListener;
    private Activity activity;

    public TipsFragment(List<Group> groups) {
        this.groups = groups;
    }
    public TipsFragment() {
    }
    public void updateList(Photos photos) {
        Log.i("TAG", "updateList: " + photos.getItems().size());
        if (adapter != null && photos != null && photos.getVenuePhotos() != null && photos.getVenuePhotos().getItems() != null) {
            adapter.addAll(photos.getVenuePhotos().getItems());
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPhotosBinding.inflate(inflater);
        initRecyclerView();

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (groups != null &&
                groups.size() > 0 &&
                groups.get(0) != null &&
                groups.get(0).getItems() != null &&
                groups.get(0).getItems().size() > 0) {

            adapter.addAll(groups.get(0).getItems());
        } else if (detailsEventListener != null) {
            detailsEventListener.loadMorePhotos(0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        this.activity = getActivity();
        detailsEventListener = (DetailsEventListener) context;
    }

    private void initRecyclerView() {
        binding.photosRecycler.setLayoutManager(new GridLayoutManager(context, 3));
        int side = (int) PhoneDisplayMetrics.getDisplayWidth(activity);
        side = (side / 3) - 8;
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.width = side;
        lp.height = side;
        lp.setMargins(8, 8, 8, 0);
        adapter = new TipsAdapter(context);
        binding.photosRecycler.setAdapter(adapter);

        MyRecyclerViewScrollListener recyclerViewScrollListener = new MyRecyclerViewScrollListener((LinearLayoutManager) binding.photosRecycler.getLayoutManager()) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (detailsEventListener != null) {
//                    detailsEventListener.loadMoreTips(totalItemsCount);
                }
            }
        };

        binding.photosRecycler.addOnScrollListener(recyclerViewScrollListener);

    }

}