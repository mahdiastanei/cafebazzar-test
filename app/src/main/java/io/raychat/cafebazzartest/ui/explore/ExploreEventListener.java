package io.raychat.cafebazzartest.ui.explore;

/**
 * Created by Alireza Nezami on 4/7/2021.
 */
public interface ExploreEventListener {

    void requestLocationPermission();

    void setButtonState(boolean isLocationRequested);

    void clearExploreDate();
}
